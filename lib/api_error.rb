module APIError
  class Exceptions
    def self.record_not_found(exception, status = 404)
      error_occurred(exception, status)
    end

    def self.record_invalid(exception, status = 422)
      error_occurred(exception, status)
    end

    def self.record_duplicate(exception, status = 500)
      error_occurred(exception, status)
    end

    def self.forbidden(exception, status = 403)
      error_occurred(exception, status)
    end

    def self.error_occurred(exception, status, additional = {})
      error = additional[:error]
      error ||= exception.class.to_s.split('::').last

      error_hash = {error: error, message: exception.message, status: status}
      error_hash[:action] = additional[:action] if additional[:action]
      error_hash[:item] = additional[:item] if additional[:item]

      error_hash
    end
  end
end