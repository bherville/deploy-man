module TicketingSystems::ActsAsTicketable
  extend ActiveSupport::Concern

  included do
  end

  module ClassMethods
    def acts_as_ticketable(options = {})
      if ticketing_system_enabled?(:jira)
        include TicketingSystems::ActsAsTicketable::LocalInstanceMethods
        before_validation :add_ticketable_permissions
      end
    end
  end

  module LocalInstanceMethods
    def ticketable_permission_enabled?(name)
      rtsp = ResourceTicketingSystemPermission.joins(:ticketing_system_permission).where('resource_ticketing_system_permissions.resource_id = ? AND ticketing_system_permissions.name = ?', id, name.to_s).limit(1).first

      rtsp ? rtsp.enabled : false
    end
  end

  def add_ticketable_permissions
    if ticketing_system_enabled?(:jira)
      BUILT_IN_TICKETING_SYSTEM_PERMISSIONS[self.class.name.underscore.to_sym].each do |tsp|
        temp_tsp = TicketingSystemPermission.find_by_name(tsp.name)

        ticketing_system_permissions << temp_tsp unless resource_ticketing_system_permissions.map(&:ticketing_system_permission_id).include?(temp_tsp.id)
      end
    end
  end
end

ActiveRecord::Base.send :include, TicketingSystems::ActsAsTicketable