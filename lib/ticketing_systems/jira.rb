module TicketingSystems
  module Jira
    END_POINT = '/rest/api/2'

    def self.add_comment(issue, comment)
      RestClient.post "#{build_base_url}/issue/#{issue}/comment", { body: comment }.to_json, { content_type: :json, accept: :json, Authorization: build_header_authentication }
    end

    def self.attach_file(issue, file_path)
      RestClient.post "#{build_base_url}/issue/#{issue}/attachments", { file: File.new(file_path, 'rb') }, { accept: :json, Authorization: build_header_authentication, 'X-Atlassian-Token' => 'no-check' }
    end

    def self.assign_issue(issue, user_to_assign)
      RestClient.put "#{build_base_url}/issue/#{issue}", { fields: { assignee: { name: user_to_assign } } }.to_json, { content_type: :json, accept: :json, Authorization: build_header_authentication }
    end

    private
    def self.build_header_authentication
      "Basic #{ Base64.encode64( "#{AppConfig.get(%w(jira username), APP_CONFIG)}:#{AppConfig.get(%w(jira password), APP_CONFIG)}" ).chomp}"
    end

    def self.build_base_url
      "#{AppConfig.get(%w(jira site), APP_CONFIG)}#{AppConfig.get(%w(jira base_path), APP_CONFIG)}#{END_POINT}"
    end
  end
end