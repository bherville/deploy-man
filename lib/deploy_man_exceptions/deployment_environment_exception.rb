module DeployManExceptions
  class DeploymentEnvironmentException < StandardError
    def initialize(msg = nil)
      @message = msg
    end

    def message
      @message
    end
  end
end