class DeployManDeploymentModule
  @deployment_environment_id

  def initialize(deployment_environment_id)
    @deployment_environment_id = deployment_environment_id
    @deployment_environment = DeploymentEnvironment.find(@deployment_environment_id)
  end

  def update_output_log(message)
    @deployment_environment.update_output_log(message)
  end
end