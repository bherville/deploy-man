class DeployManDeploymentModuleExample < DeployManDeploymentModule
  def pre_deploy(deployment_hash)

    { success: true }
  end

  def deploy(deployment_hash)
    200.times.each do |n|
      update_output_log("Writing: #{n.to_s}")
    end

    { success: true }
  end

  def post_deploy(deployment_hash)

    { success: true }
  end
end