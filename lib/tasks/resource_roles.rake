namespace :resource_roles do
  desc 'Refreshes the built in ResourceRole associations'
  task refresh: :environment do
    ResourceRole.joins(:role).where('roles.built_in = ?', true).destroy_all

    create_default_resource_roles
  end
end
