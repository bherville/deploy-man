# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151105163039) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "applications", force: :cascade do |t|
    t.string   "name",                                  null: false
    t.integer  "user_id",                               null: false
    t.integer  "deployment_module_id",                  null: false
    t.integer  "approval_count",        default: 0,     null: false
    t.boolean  "allow_self_approval",   default: false
    t.text     "deployment_parameters"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "applications", ["name"], name: "index_applications_on_name", using: :btree
  add_index "applications", ["user_id"], name: "index_applications_on_user_id", using: :btree

  create_table "approval_group_applications", force: :cascade do |t|
    t.integer  "approval_group_id", null: false
    t.integer  "application_id",    null: false
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "approval_group_applications", ["application_id"], name: "index_approval_group_applications_on_application_id", using: :btree
  add_index "approval_group_applications", ["approval_group_id"], name: "index_approval_group_applications_on_approval_group_id", using: :btree

  create_table "approval_group_users", force: :cascade do |t|
    t.integer  "approval_group_id"
    t.integer  "user_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "approval_group_users", ["approval_group_id"], name: "index_approval_group_users_on_approval_group_id", using: :btree
  add_index "approval_group_users", ["user_id"], name: "index_approval_group_users_on_user_id", using: :btree

  create_table "approval_groups", force: :cascade do |t|
    t.string   "name",        null: false
    t.string   "description"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "approval_groups", ["name"], name: "index_approval_groups_on_name", using: :btree

  create_table "approvals", force: :cascade do |t|
    t.string   "workflow_state"
    t.integer  "user_id"
    t.integer  "approver_user_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.text     "approver_comment"
    t.integer  "deployment_environment_id"
  end

  add_index "approvals", ["approver_user_id"], name: "index_approvals_on_approver_user_id", using: :btree
  add_index "approvals", ["deployment_environment_id"], name: "index_approvals_on_deployment_environment_id", using: :btree

  create_table "audits", force: :cascade do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         default: 0
    t.string   "comment"
    t.string   "remote_address"
    t.string   "request_uuid"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], name: "associated_index", using: :btree
  add_index "audits", ["auditable_id", "auditable_type"], name: "auditable_index", using: :btree
  add_index "audits", ["created_at"], name: "index_audits_on_created_at", using: :btree
  add_index "audits", ["request_uuid"], name: "index_audits_on_request_uuid", using: :btree
  add_index "audits", ["user_id", "user_type"], name: "user_index", using: :btree

  create_table "dashboards", force: :cascade do |t|
    t.string   "name",                           null: false
    t.text     "description"
    t.string   "view_path"
    t.text     "view_html"
    t.boolean  "public",         default: false, null: false
    t.boolean  "built_in",       default: false, null: false
    t.boolean  "system_default", default: false, null: false
    t.string   "slug"
    t.integer  "user_id"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "dashboards", ["name"], name: "index_dashboards_on_name", using: :btree
  add_index "dashboards", ["slug"], name: "index_dashboards_on_slug", unique: true, using: :btree

  create_table "deployment_environments", force: :cascade do |t|
    t.integer  "environment_id",   null: false
    t.integer  "deployment_id",    null: false
    t.string   "workflow_state"
    t.text     "output_log"
    t.text     "deployer_comment"
    t.integer  "deployer_user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.datetime "deploy_at"
    t.string   "deploy_at_job_id"
  end

  add_index "deployment_environments", ["deployer_user_id"], name: "index_deployment_environments_on_deployer_user_id", using: :btree
  add_index "deployment_environments", ["environment_id", "deployment_id"], name: "deployment_environments_on_environment_id_and_deployment_id", using: :btree

  create_table "deployment_modules", force: :cascade do |t|
    t.string   "name",                            null: false
    t.text     "settings"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.text     "description"
    t.string   "module_path"
    t.string   "module_version"
    t.text     "module_files"
    t.text     "default_parameters_applications"
    t.text     "default_parameters_environments"
    t.text     "default_parameters_deployments"
    t.text     "default_settings"
  end

  add_index "deployment_modules", ["name"], name: "index_deployment_modules_on_name", using: :btree

  create_table "deployment_templates", force: :cascade do |t|
    t.string   "name",                                                  null: false
    t.text     "comment"
    t.text     "deployment_parameters"
    t.boolean  "public",                                default: false
    t.integer  "user_id"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "ticket"
    t.integer  "application_id"
    t.string   "post_deployment_ticketing_system_user"
  end

  add_index "deployment_templates", ["name"], name: "index_deployment_templates_on_name", using: :btree
  add_index "deployment_templates", ["user_id", "name"], name: "index_deployment_templates_on_user_id_and_name", unique: true, using: :btree
  add_index "deployment_templates", ["user_id"], name: "index_deployment_templates_on_user_id", using: :btree

  create_table "deployments", force: :cascade do |t|
    t.text     "comment"
    t.string   "ticket"
    t.string   "deployment_parameters"
    t.text     "log"
    t.integer  "application_id",                                        null: false
    t.integer  "user_id",                                               null: false
    t.integer  "deployer_user_id"
    t.datetime "created_at",                                            null: false
    t.datetime "updated_at",                                            null: false
    t.string   "deployer_comment"
    t.integer  "deployment_template_id"
    t.string   "workflow_state"
    t.string   "post_deployment_ticketing_system_user"
    t.boolean  "akamai_cache_purge",                    default: false
  end

  add_index "deployments", ["application_id"], name: "index_deployments_on_application_id", using: :btree
  add_index "deployments", ["deployer_user_id"], name: "index_deployments_on_deployer_user_id", using: :btree

  create_table "environments", force: :cascade do |t|
    t.string   "name",                              null: false
    t.text     "deployment_parameters"
    t.integer  "deployment_order",      default: 1, null: false
    t.integer  "application_id",                    null: false
    t.integer  "user_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.text     "akamai_cp_codes"
  end

  add_index "environments", ["application_id"], name: "index_environments_on_application_id", using: :btree
  add_index "environments", ["deployment_order", "application_id"], name: "index_environments_on_deployment_order_and_application_id", unique: true, using: :btree
  add_index "environments", ["name"], name: "index_environments_on_name", using: :btree

  create_table "friendly_id_slugs", force: :cascade do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "mailer_notification_permissions", force: :cascade do |t|
    t.string   "name",                    null: false
    t.string   "description"
    t.integer  "built_in",    default: 0, null: false
    t.string   "mailer_name",             null: false
    t.string   "method_name",             null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "mailer_notification_permissions", ["mailer_name"], name: "index_mailer_notification_permissions_on_mailer_name", using: :btree
  add_index "mailer_notification_permissions", ["name"], name: "index_mailer_notification_permissions_on_name", using: :btree

  create_table "resource_cc_users", force: :cascade do |t|
    t.integer  "ccable_id"
    t.string   "ccable_type"
    t.integer  "user_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "resource_cc_users", ["ccable_id", "ccable_type", "user_id"], name: "index_resource_cc_users_on_ccable_and_user_id", unique: true, using: :btree
  add_index "resource_cc_users", ["ccable_type", "ccable_id"], name: "index_resource_cc_users_on_ccable_type_and_ccable_id", using: :btree

  create_table "resource_roles", force: :cascade do |t|
    t.string   "resource_name",                    null: false
    t.string   "resource_type", default: "string", null: false
    t.integer  "role_id",                          null: false
    t.text     "permissions",                      null: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "resource_roles", ["resource_type"], name: "index_resource_roles_on_resource_type", using: :btree
  add_index "resource_roles", ["role_id"], name: "index_resource_roles_on_role_id", using: :btree

  create_table "resource_ticketing_system_permissions", force: :cascade do |t|
    t.integer  "resource_id"
    t.string   "resource_type"
    t.integer  "ticketing_system_permission_id"
    t.boolean  "enabled",                        default: true
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
  end

  add_index "resource_ticketing_system_permissions", ["resource_type", "resource_id"], name: "resource_ticketing_system_permissions_on_resource", using: :btree

  create_table "roles", force: :cascade do |t|
    t.string   "name",                        null: false
    t.string   "description"
    t.boolean  "built_in",    default: false, null: false
    t.integer  "user_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "roles", ["name"], name: "index_roles_on_name", using: :btree
  add_index "roles", ["user_id"], name: "index_roles_on_user_id", using: :btree

  create_table "ticketing_system_permissions", force: :cascade do |t|
    t.string   "name",                        null: false
    t.string   "description"
    t.boolean  "built_in",    default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  add_index "ticketing_system_permissions", ["name"], name: "index_ticketing_system_permissions_on_name", using: :btree

  create_table "user_mailer_notification_permissions", force: :cascade do |t|
    t.integer  "mailer_notification_permission_id",                null: false
    t.integer  "user_id",                                          null: false
    t.boolean  "send_notification",                 default: true, null: false
    t.datetime "created_at",                                       null: false
    t.datetime "updated_at",                                       null: false
  end

  add_index "user_mailer_notification_permissions", ["mailer_notification_permission_id"], name: "user_mailer_notification_permission_id", using: :btree
  add_index "user_mailer_notification_permissions", ["user_id"], name: "user_mailer_notification_permission_user_id", using: :btree

  create_table "user_roles", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "user_roles", ["role_id"], name: "index_user_roles_on_role_id", using: :btree
  add_index "user_roles", ["user_id"], name: "index_user_roles_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "fname",                               null: false
    t.string   "lname",                               null: false
    t.string   "provider"
    t.integer  "uid"
    t.string   "time_zone"
    t.string   "email",                  default: "", null: false
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.string   "authentication_token"
    t.integer  "default_dashboard_id"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["provider"], name: "index_users_on_provider", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["uid"], name: "index_users_on_uid", using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree

  create_table "watchers", force: :cascade do |t|
    t.integer  "watchable_id"
    t.string   "watchable_type"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  add_index "watchers", ["watchable_id", "watchable_type", "user_id"], name: "index_watchers_on_watchable_id_and_watchable_type_and_user_id", unique: true, using: :btree
  add_index "watchers", ["watchable_type", "watchable_id"], name: "index_watchers_on_watchable_type_and_watchable_id", using: :btree

end
