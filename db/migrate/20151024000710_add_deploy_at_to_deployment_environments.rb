class AddDeployAtToDeploymentEnvironments < ActiveRecord::Migration
  def change
    add_column :deployment_environments, :deploy_at, :timestamp
    add_column :deployment_environments, :deploy_at_job_id, :string
  end
end
