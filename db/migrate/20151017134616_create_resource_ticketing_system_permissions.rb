class CreateResourceTicketingSystemPermissions < ActiveRecord::Migration
  def change
    create_table :resource_ticketing_system_permissions do |t|
      t.integer :resource_id
      t.string :resource_type
      t.integer :ticketing_system_permission_id
      t.boolean :enabled, default: true

      t.timestamps null: false
    end

    add_index :resource_ticketing_system_permissions, [:resource_type, :resource_id], name: :resource_ticketing_system_permissions_on_resource
  end
end
