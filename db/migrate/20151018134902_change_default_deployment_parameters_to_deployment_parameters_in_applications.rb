class ChangeDefaultDeploymentParametersToDeploymentParametersInApplications < ActiveRecord::Migration
  def change
    rename_column :applications, :default_deployment_parameters, :deployment_parameters
  end
end
