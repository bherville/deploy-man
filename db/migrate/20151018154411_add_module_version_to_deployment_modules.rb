class AddModuleVersionToDeploymentModules < ActiveRecord::Migration
  def change
    add_column :deployment_modules, :module_version, :string
  end
end
