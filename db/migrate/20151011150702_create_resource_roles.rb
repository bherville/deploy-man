class CreateResourceRoles < ActiveRecord::Migration
  def change
    create_table :resource_roles do |t|
      t.string  :resource_name, null: false
      t.string  :resource_type, null: false, default: :string
      t.integer :role_id, null: false
      t.text    :permissions, null: false

      t.timestamps null: false
    end

    add_index :resource_roles, :resource_type
    add_index :resource_roles, :role_id
  end
end
