class CreateApprovals < ActiveRecord::Migration
  def change
    create_table :approvals do |t|
      t.string  :workflow_state
      t.integer :user_id
      t.integer :approver_user_id

      t.timestamps null: false
    end

    add_index :approvals, :approver_user_id
  end
end
