class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.string  :name, null: false
      t.string  :description
      t.boolean :built_in, default: false, null: false
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :roles, :name
    add_index :roles, :user_id
  end
end
