class AddModuleFilesToDeploymentModules < ActiveRecord::Migration
  def change
    add_column :deployment_modules, :module_files, :text
  end
end
