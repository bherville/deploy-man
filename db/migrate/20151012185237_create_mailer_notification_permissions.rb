class CreateMailerNotificationPermissions < ActiveRecord::Migration
  def change
    create_table :mailer_notification_permissions do |t|
      t.string  :name, null: false
      t.string  :description
      t.integer :built_in, null: false, default: false
      t.string  :mailer_name, null: false
      t.string  :method_name, null: false

      t.timestamps null: false
    end

    add_index :mailer_notification_permissions, :name
    add_index :mailer_notification_permissions, :mailer_name
  end
end
