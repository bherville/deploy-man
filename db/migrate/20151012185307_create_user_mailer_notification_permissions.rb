class CreateUserMailerNotificationPermissions < ActiveRecord::Migration
  def change
    create_table :user_mailer_notification_permissions do |t|
      t.integer :mailer_notification_permission_id, null: false
      t.integer :user_id, null: false
      t.boolean :send_notification, null: false, default: true

      t.timestamps null: false
    end

    add_index :user_mailer_notification_permissions, :mailer_notification_permission_id, name: :user_mailer_notification_permission_id
    add_index :user_mailer_notification_permissions, :user_id, name: :user_mailer_notification_permission_user_id
  end
end
