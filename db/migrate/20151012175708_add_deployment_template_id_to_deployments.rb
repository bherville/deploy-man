class AddDeploymentTemplateIdToDeployments < ActiveRecord::Migration
  def change
    add_column :deployments, :deployment_template_id, :integer
  end
end
