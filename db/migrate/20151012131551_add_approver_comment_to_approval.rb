class AddApproverCommentToApproval < ActiveRecord::Migration
  def change
    add_column :approvals, :approver_comment, :text
  end
end
