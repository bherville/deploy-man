class AddUniqIndexToDeploymentTemplates < ActiveRecord::Migration
  def change
    add_index :deployment_templates, [:user_id, :name], unique: true
  end
end
