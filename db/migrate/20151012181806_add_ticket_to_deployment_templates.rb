class AddTicketToDeploymentTemplates < ActiveRecord::Migration
  def change
    add_column :deployment_templates, :ticket, :string
  end
end
