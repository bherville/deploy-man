class CreateDeploymentTemplates < ActiveRecord::Migration
  def change
    create_table :deployment_templates do |t|
      t.string :name, null: false
      t.text :comment
      t.text :deployment_parameters
      t.boolean :public, default: false
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :deployment_templates, :name
    add_index :deployment_templates, :user_id
  end
end
