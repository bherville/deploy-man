class AddApplicationIdToDeploymentTemplates < ActiveRecord::Migration
  def change
    add_column :deployment_templates, :application_id, :integer
  end
end
