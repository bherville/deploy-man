class AddDescriptionAndModulePathToDeploymentModules < ActiveRecord::Migration
  def change
    add_column :deployment_modules, :description, :text
    add_column :deployment_modules, :module_path, :string
  end
end
