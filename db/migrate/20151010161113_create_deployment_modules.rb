class CreateDeploymentModules < ActiveRecord::Migration
  def change
    create_table :deployment_modules do |t|
      t.string :name, null: false
      t.text :settings

      t.timestamps null: false
    end
    add_index :deployment_modules, :name
  end
end
