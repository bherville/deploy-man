class AddPostDeploymentTicketingSystemUserToDeploymentTemplates < ActiveRecord::Migration
  def change
    add_column :deployment_templates, :post_deployment_ticketing_system_user, :string
  end
end
