class AddAkamaiCachePurgeToDeployments < ActiveRecord::Migration
  def change
    add_column :deployments, :akamai_cache_purge, :boolean, default: false
  end
end
