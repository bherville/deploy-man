class CreateDeploymentEnvironments < ActiveRecord::Migration
  def change
    create_table :deployment_environments do |t|
      t.integer :environment_id, null: false
      t.integer :deployment_id, null: false
      t.string  :workflow_state
      t.text    :output_log
      t.text    :deployer_comment
      t.integer :deployer_user_id

      t.timestamps null: false
    end

    add_index :deployment_environments, [:environment_id, :deployment_id], name: :deployment_environments_on_environment_id_and_deployment_id
    add_index :deployment_environments, :deployer_user_id
  end
end
