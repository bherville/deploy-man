class CreateApprovalGroupUsers < ActiveRecord::Migration
  def change
    create_table :approval_group_users do |t|
      t.integer :approval_group_id
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :approval_group_users, :approval_group_id
    add_index :approval_group_users, :user_id
  end
end
