class CreateDeployments < ActiveRecord::Migration
  def change
    create_table :deployments do |t|
      t.text    :comment
      t.string  :ticket
      t.string  :deployment_parameters
      t.text    :log

      t.integer :application_id, null: false
      t.integer :user_id, null: false
      t.integer :deployer_user_id

      t.timestamps null: false
    end

    add_index :deployments, :application_id
    add_index :deployments, :deployer_user_id
  end
end
