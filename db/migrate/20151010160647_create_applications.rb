class CreateApplications < ActiveRecord::Migration
  def change
    create_table :applications do |t|
      t.string  :name, null: false
      t.integer :user_id, null: false
      t.integer :deployment_module_id, null: false
      t.integer :approval_count, null: false, default: 0
      t.boolean :allow_self_approval, default: false

      t.text :default_deployment_parameters

      t.timestamps null: false
    end

    add_index :applications, :name
    add_index :applications, :user_id
  end
end
