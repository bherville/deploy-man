class CreateApprovalGroupApplications < ActiveRecord::Migration
  def change
    create_table :approval_group_applications do |t|
      t.integer :approval_group_id, null: false
      t.integer :application_id, null: false

      t.timestamps null: false
    end

    add_index :approval_group_applications, :approval_group_id
    add_index :approval_group_applications, :application_id
  end
end
