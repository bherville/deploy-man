class AddDefaultParametersToDeploymentModules < ActiveRecord::Migration
  def change
    add_column :deployment_modules, :default_parameters_applications, :text
    add_column :deployment_modules, :default_parameters_environments, :text
    add_column :deployment_modules, :default_parameters_deployments, :text
    add_column :deployment_modules, :default_settings, :text
  end
end
