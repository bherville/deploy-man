class AddDeployerCommentToDeployment < ActiveRecord::Migration
  def change
    add_column :deployments, :deployer_comment, :string
  end
end
