class CreateApprovalGroups < ActiveRecord::Migration
  def change
    create_table :approval_groups do |t|
      t.string  :name, null: false
      t.string  :description
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :approval_groups, :name
  end
end
