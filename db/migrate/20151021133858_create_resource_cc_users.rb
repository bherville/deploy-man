class CreateResourceCcUsers < ActiveRecord::Migration
  def change
    create_table :resource_cc_users do |t|
      t.references :ccable, polymorphic: true, index: true
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :resource_cc_users, [:ccable_id, :ccable_type, :user_id], unique: true, name: 'index_resource_cc_users_on_ccable_and_user_id'
  end
end
