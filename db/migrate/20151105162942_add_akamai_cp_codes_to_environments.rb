class AddAkamaiCpCodesToEnvironments < ActiveRecord::Migration
  def change
    add_column :environments, :akamai_cp_codes, :text
  end
end
