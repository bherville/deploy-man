class CreateTicketingSystemPermissions < ActiveRecord::Migration
  def change
    create_table :ticketing_system_permissions do |t|
      t.string :name, null: false
      t.string :description
      t.boolean :built_in, default: false

      t.timestamps null: false
    end

    add_index :ticketing_system_permissions, :name
  end
end
