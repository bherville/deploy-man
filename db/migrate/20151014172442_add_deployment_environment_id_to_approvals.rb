class AddDeploymentEnvironmentIdToApprovals < ActiveRecord::Migration
  def change
    add_column :approvals, :deployment_environment_id, :integer

    add_index :approvals, :deployment_environment_id
  end
end
