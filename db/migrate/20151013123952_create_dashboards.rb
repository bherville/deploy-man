class CreateDashboards < ActiveRecord::Migration
  def change
    create_table :dashboards do |t|
      t.string  :name, null: false
      t.text    :description
      t.string  :view_path
      t.text    :view_html
      t.boolean :public, null: false, default: false
      t.boolean :built_in, null: false, default: false
      t.boolean :system_default, null: false, default: false
      t.string  :slug
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :dashboards, :name
    add_index :dashboards, :slug, unique: true
  end
end
