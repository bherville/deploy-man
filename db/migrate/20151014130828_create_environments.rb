class CreateEnvironments < ActiveRecord::Migration
  def change
    create_table :environments do |t|
      t.string  :name, null: false
      t.text    :deployment_parameters
      t.integer :deployment_order, null: false, default: 1
      t.integer :application_id, null: false
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :environments, :name
    add_index :environments, :application_id
    add_index :environments, [:deployment_order, :application_id], unique: true
  end
end
