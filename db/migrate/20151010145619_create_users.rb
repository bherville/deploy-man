class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string  :fname,    null: false
      t.string  :lname,    null: false
      t.string  :provider
      t.integer :uid
      t.string  :time_zone
      t.string  :email,    null: false, default: ""

      t.timestamps null: false
    end

    add_index :users, :provider
    add_index :users, :uid
    add_index :users, :email
  end
end
