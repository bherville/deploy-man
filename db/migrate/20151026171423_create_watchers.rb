class CreateWatchers < ActiveRecord::Migration
  def change
    create_table :watchers do |t|
      t.references :watchable, polymorphic: true, index: true
      t.integer :user_id

      t.timestamps null: false
    end

    add_index :watchers, [:watchable_id, :watchable_type, :user_id], unique: true
  end
end
