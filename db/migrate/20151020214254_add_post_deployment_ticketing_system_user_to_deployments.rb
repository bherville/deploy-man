class AddPostDeploymentTicketingSystemUserToDeployments < ActiveRecord::Migration
  def change
    add_column :deployments, :post_deployment_ticketing_system_user, :string
  end
end
