class ResourceCcUser < ActiveRecord::Base
  belongs_to :ccable, polymorphic: true

  belongs_to :user
end
