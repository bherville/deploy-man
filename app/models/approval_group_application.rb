class ApprovalGroupApplication < ActiveRecord::Base
  audited

  belongs_to :application
  belongs_to :approval_group

  validates :approval_group_id, presence: true
  validates_uniqueness_of :application_id, scope: :approval_group_id
end
