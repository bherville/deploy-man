class Admin
  ADMIN_PAGES = {
      :approval_groups => {
          :title        => I18n.t('approval_groups.approval_groups'),
          :path         => Rails.application.routes.url_helpers.admin_approval_groups_path,
          :description  => I18n.t('admin.manage_approval_groups')
      },
      :dashboards => {
          :title        => I18n.t('dashboards.dashboards'),
          :path         => Rails.application.routes.url_helpers.admin_dashboards_path,
          :description  => I18n.t('admin.manage_dashboards')
      },
      :deployment_modules => {
          :title        => I18n.t('deployment_modules.deployment_modules'),
          :path         => Rails.application.routes.url_helpers.admin_deployment_modules_path,
          :description  => I18n.t('admin.manage_deployment_modules')
      },
      :mailer_notification_permissions => {
          :title        => I18n.t('mailer_notification_permissions.mailer_notification_permissions'),
          :path         => Rails.application.routes.url_helpers.admin_mailer_notification_permissions_path,
          :description  => I18n.t('admin.manage_mailer_notification_permissions')
      },
      :roles => {
          :title        => I18n.t('roles.roles'),
          :path         => Rails.application.routes.url_helpers.admin_roles_path,
          :description  => I18n.t('admin.manage_roles')
      },
      :users => {
          :title        => I18n.t('user.users'),
          :path         => Rails.application.routes.url_helpers.admin_users_path,
          :description  => I18n.t('admin.manage_users')
      }
  }
end