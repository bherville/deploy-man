class ApprovalGroupUser < ActiveRecord::Base
  audited

  belongs_to :approval_group
  belongs_to :user

  validates :user_id, presence: true
  validates_uniqueness_of :user_id, scope: :approval_group_id
end
