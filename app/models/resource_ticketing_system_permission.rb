class ResourceTicketingSystemPermission < ActiveRecord::Base
  belongs_to :resource, polymorphic: true

  belongs_to :ticketing_system_permission
end
