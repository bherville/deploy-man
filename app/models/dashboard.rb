class Dashboard < ActiveRecord::Base
  audited

  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :user

  has_many :users, foreign_key: 'default_dashboard_id', class_name: 'User'

  validates :name, presence: true, uniqueness: true
  validate :valid_view?

  before_save :check_system_default

  def self.system_default_dashboard
    defaults = Dashboard.where(system_default: true)

    if defaults.count > 0
      defaults.first
    else
      SYSTEM_DEFAULT_DASHBOARD
    end
  end

  private
  def valid_view?
    unless (self.view_html.nil? ^ self.view_path.nil?) || self.view_html.empty? ^ self.view_path.empty?
      errors.add(:base, 'Either View HTML or View Path must be set at a time')
    end
  end

  def check_system_default
    if self.system_default
      Dashboard.where(system_default: true).each do |dashboard|
        dashboard.system_default = false
        dashboard.save
      end
    end
  end
end
