class User < ActiveRecord::Base
  include Gravtastic

  audited
  acts_as_token_authenticatable

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :confirmable, :lockable, :timeoutable

  devise :omniauthable, :omniauth_providers => [:auth_man]

  gravtastic :secure => (AppConfig.get(%w(user_profiles secure), APP_CONFIG) ? AppConfig.get(%w(user_profiles secure), APP_CONFIG) : false)

  before_create :add_default_roles

  after_create :send_admin_emails
  after_save :add_default_mailer_notification_permissions


  has_many :applications
  has_many :approval_group_users
  has_many :deployment_templates
  has_many :dashboards

  has_many :user_roles, dependent: :delete_all
  has_many :user_mailer_notification_permissions, dependent: :delete_all

  has_many :resource_cc_user, dependent: :destroy
  has_many :watchers, dependent: :destroy

  has_many :created_deployments, foreign_key: 'user_id', class_name: 'Deployment'
  has_many :approvals, foreign_key: 'approver_user_id'
  has_many :deployment_environments, foreign_key: 'deployer_user_id', class_name: 'DeploymentEnvironment'

  has_many :roles, :through => :user_roles
  has_many :approval_groups, :through => :approval_group_users
  has_many :watching, :through => :watchers, :source => :user

  belongs_to :default_dashboard, foreign_key: 'default_dashboard_id', class_name: 'Dashboard'

  accepts_nested_attributes_for :user_mailer_notification_permissions
  accepts_nested_attributes_for :user_roles
  accepts_nested_attributes_for :roles

  validates :fname, presence: true
  validates :lname, presence: true

  def to_s
    self.name
  end

  def self.from_omniauth(auth)
    user_return = where(provider: auth.provider, uid: auth.uid).first

    unless user_return
      user = User.new(
          :email        => auth.info.email,
          :password     => Devise.friendly_token[0,20],
          :fname        => auth.info.fname,
          :lname        => auth.info.lname,
          :time_zone    => auth.info.time_zone,
      )

      user.uid      = auth.uid
      user.provider = auth.provider

      user.skip_confirmation!
      user.save

      user_return = user
    end

    user_return.sync_with_omniauth(auth)

    user_return
  end

  def self.new_with_session(params, session)
    super.tap do |user|
      if data = session['devise.auth_man_data'] && session['devise.auth_man_data']['extra']['raw_info']
        user.email      = data['email'] if user.email.blank?
        user.fname      = data['fname'] if user.fname.blank?
        user.lname      = data['lname'] if user.lname.blank?
        user.time_zone  = data['time_zone'] if user.time_zone.blank?
      end
    end
  end

  def sync_with_omniauth(auth)
    unless auth.info.fname == self.fname && auth.info.lname == self.lname && auth.info.time_zone == self.time_zone
      self.fname      = auth.info.fname
      self.lname      = auth.info.lname
      self.time_zone  = auth.info.time_zone

      self.save
    end
  end

  def password_required?
    super if confirmed?
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def full_name
    self.fname && self.lname ? "#{self.fname} #{self.lname}" : nil
  end

  def full_name_with_email
    self.fname && self.lname ? "#{self.fname} #{self.lname} - #{self.email}" : nil
  end

  def full_name_comma
    self.fname && self.lname ? "#{self.lname}, #{self.fname}" : nil
  end

  def full_name_comma_with_email
    self.fname && self.lname ? "#{self.lname}, #{self.fname} - #{self.email}" : nil
  end

  def name
    self.fname && self.lname ? "#{self.fname} #{self.lname}" : self.email
  end

  def has_role?(role)
    case role
      when String, Symbol
        role_to_find = Role.find_by_name(role.to_s)
      when Role
        role_to_find = role
      else
        return false
    end

    self.roles.include?(role_to_find)
  end

  def add_roles(*roles)
    roles.each do |role|
      unless self.has_role?(role)
        self.roles << Role.find_by_name(role)
      end
    end

    self.save!
  end

  def approvals_queue
    deploys = self.approval_groups.map(&:applications).flatten.map(&:deployments).flatten

    approvals_queued = []

    deploys.each do |deploy|
      unless deploy.user_id == self.id && (!deploy.application.allow_self_approval? && !self.has_role?(:approval_admin))
        approvals_queued << deploys.flatten.select { |d| !d.approved? }.map(&:approvals).flatten.select { |a| a.status.to_sym == :waiting }
      end
    end

    approvals_queued
  end

  def watching?(resource)
    defined?(resource.watchers) && !resource.watchers.find_by_user_id(self.id).nil?
  end

  private
  def valid_roles
    if self.roles_mask.nil? || self.roles_mask < 1
      errors.add(:base, 'At least one role must be selected')
    end
  end

  def add_default_roles
    DEFAULT_ROLES.each do |role|
      unless self.has_role?(role)
        self.roles << Role.find(role.id)
      end
    end
  end

  def add_default_mailer_notification_permissions
    BUILT_IN_MAILER_NOTIFICATION_PERMISSIONS.each do |mnp|
      self.user_mailer_notification_permissions.create(:mailer_notification_permission_id => mnp.id, :send_notification => true) unless self.user_mailer_notification_permissions.find_by_mailer_notification_permission_id(mnp.id)
    end
  end

  def send_admin_emails
    AdminMailer.delay.new_user_notification(self.id)
  end
end
