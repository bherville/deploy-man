class ApprovalGroup < ActiveRecord::Base
  audited

  has_many :approval_group_applications, dependent: :delete_all
  has_many :approval_group_users, dependent: :delete_all

  has_many :applications, :through => :approval_group_applications
  has_many :users, :through => :approval_group_users

  accepts_nested_attributes_for :approval_group_users

  validates :name, presence: true, uniqueness: true
end
