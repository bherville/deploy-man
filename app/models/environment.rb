class Environment < ActiveRecord::Base
  audited associated_with: :application

  has_many :deployment_environments, dependent: :destroy

  has_many :deployments, through: :deployment_environments

  belongs_to :application


  validates :name, presence: true
  validates :deployment_order, presence: true

  validates_numericality_of :deployment_order, greater_than: 0

  validates_uniqueness_of :name, scope: :application_id

  before_save :akamai_cp_codes_to_array

  serialize :deployment_parameters, Hash
  serialize :akamai_cp_codes


  private
  def akamai_cp_codes_to_array
    if self.akamai_cp_codes.is_a?(String)
      self.akamai_cp_codes = self.akamai_cp_codes.split(',')
    end
  end
end