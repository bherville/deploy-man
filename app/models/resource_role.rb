class ResourceRole < ActiveRecord::Base
  audited

  ALL_CRUD_PERMISSIONS = [
      :create,
      :read,
      :update,
      :destroy
  ]

  belongs_to :role

  serialize :permissions, Array
end
