class UserRole < ActiveRecord::Base
  audited

  belongs_to :role
  belongs_to :user

  validates :role_id, presence: true
  validates :user_id, presence: true
  validates_uniqueness_of :user_id, scope: :role_id
end
