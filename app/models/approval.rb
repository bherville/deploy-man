class Approval < ActiveRecord::Base
  include PublicActivity::Model
  include Workflow

  audited associated_with: :deployment_environment

  tracked owner: proc {|controller, model| controller.current_user},
          only: [:approve, :reject],
          params: {
              summary: proc {|controller, model| controller.truncate(model.approver_comment, length: 30)}
          }

  belongs_to :user
  belongs_to :approver, class_name: 'User', foreign_key: 'approver_user_id'
  belongs_to :deployment_environment

  has_one :deployment, through: :deployment_environment
  has_one :environment, through: :deployment_environment

  after_commit :send_initial_email, :on => :create

  workflow do
    state :awaiting_approval do
      event :approve,  transitions_to: :approved
      event :reject,  transitions_to: :rejected
    end
    state :approved
    state :rejected
    on_transition do |from, to, triggering_event, *event_args|
      ApprovalMailer.initialize_notification(self, to) unless to == :awaiting_approval
    end
  end

  def approve(user, approver_comment)
    self.approver_user_id = user.id
    self.approver_comment = approver_comment

    self.save

    self.create_activity key: 'approval.approved', owner: user
  end

  def reject(user, approver_comment)
    self.approver_user_id = user.id
    self.approver_comment = approver_comment

    self.save

    self.create_activity key: 'approval.rejected', owner: user
  end

  def can_approve_reject?(user)
    return false unless (self.deployment.application.approvers.include?(user) || user.has_role?(:approval_admin))

    if self.deployment.user == user
      self.deployment.application.allow_self_approval? || user.has_role?(:approval_admin)
    else
      true
    end
  end

  def already_approved_rejected?
    self.approved? || self.rejected?
  end

  def self.approvals_for_user(user)
    user_approvals = []

    Approval.where(workflow_state: 'awaiting_approval').each do |approval|
      user_approvals << approval if approval.can_approve_reject?(user)
    end

    user_approvals
  end

  private
  def send_initial_email
    ApprovalMailer.initialize_notification(self, self.current_state.to_s.to_sym)
  end
end
