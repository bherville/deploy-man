require 'sidekiq/api'

class DeploymentEnvironment < ActiveRecord::Base
  include PublicActivity::Model
  include Workflow

  tracked owner: proc {|controller, model| controller.current_user},
          only: [:deploy],
          params: {
              summary: proc {|controller, model| controller.truncate(model.deployer_comment, length: 30)}
          }

  audited except: [:output_log], associated_with: :deployment
  has_associated_audits

  belongs_to :deployment
  belongs_to :environment
  belongs_to :deployer, class_name: 'User', foreign_key: 'deployer_user_id'

  has_many :approvals, dependent: :destroy

  after_create :set_initial_state
  before_create :setup_approvals
  before_create :setup_output_log

  before_update :schedule_deployment

  serialize :output_log, Array

  workflow do
    state :new do
      event :submit, transitions_to: :awaiting_approval
    end
    state :awaiting_approval do
      event :approve, transitions_to: :awaiting_deployment,
                      if: proc { |deployment_environment| deployment_environment.approved? }
      event :reject, transitions_to: :rejected
    end
    state :awaiting_deployment do
      event :queue, transitions_to: :queued,
                    if: proc { |deployment_environment| deployment_environment.preceding_environment.nil? || DeploymentEnvironment.find_by_deployment_id_and_environment_id(deployment_environment.deployment.id, deployment_environment.preceding_environment.id).deployed? }
      event :schedule, transitions_to: :scheduled,
            if: proc { |deployment_environment| deployment_environment.preceding_environment.nil? || DeploymentEnvironment.find_by_deployment_id_and_environment_id(deployment_environment.deployment.id, deployment_environment.preceding_environment.id).deployed? }
    end
    state :rejected
    state :queued do
      event :process, transitions_to: :deploying
    end
    state :scheduled do
      event :schedule, transitions_to: :scheduled
      event :cancel_schedule, transitions_to: :awaiting_deployment
      event :process, transitions_to: :deploying
    end
    state :deploying do
      event :deploy_succeeded, transitions_to: :deployed
      event :deploy_failed, transitions_to: :failed
    end
    state :deployed do
      event :clear_akamai_cache, transitions_to: :akamai_cache_clearing
    end
    state :failed
    state :akamai_cache_clearing do
      event :akamai_cache_cleared, transitions_to: :akamai_cache_cleared
    end
    state :akamai_cache_cleared

    on_transition do |from, to, triggering_event, *event_args|
      DeploymentEnvironmentsMailer.initialize_notification(self.id, to)

      if to == :deployed || to == :failed
        if ticketing_system_enabled?(:jira) && !self.deployment.ticket.blank? && self.ticketing_system_permissions?([:comment, "post_log_on_deployment_#{to}".to_sym])
          TicketingSystemAttachmentWorker.perform_async(self.id, self.class.name, to, (self.ticketing_system_permissions?([:comment, "comment_on_deployment_#{to}".to_sym])), self.deployment.post_deployment_ticketing_system_user)
        end
      elsif to == :akamai_cache_clearing
        if AppConfig.get(%W(akamai_purge enable), APP_CONFIG) && self.deployment.akamai_cache_purge? && self.environment.akamai_cp_codes.count > 0
          AkamaiPurgeInitializeWorker.perform_async(self.id)
          TicketingSystemCommentWorker.perform_async(self.id, self.class.name, to) if ticketing_system_enabled?(:jira) && !self.deployment.ticket.blank? && self.ticketing_system_permissions?([:comment, "comment_on_deployment_#{to}".to_sym])
        end
      else
        TicketingSystemCommentWorker.perform_async(self.id, self.class.name, to) if ticketing_system_enabled?(:jira) && !self.deployment.ticket.blank? && self.ticketing_system_permissions?([:comment, "comment_on_deployment_#{to}".to_sym])
      end
    end
  end

  def approved?
    self.approvals.select { |a| a.approved? }.count == self.approvals.count
  end

  def rejected?
    (self.approvals.select { |a| a.rejected? }.count > 0) && !awaiting_approval?
  end

  def awaiting_approval?
    self.approvals.select { |a| a.awaiting_approval? }.count > 0
  end

  def deploy(user, deployer_comment)
    self.deployer_user_id = user.id
    self.deployer_comment = deployer_comment

    self.save

    self.queue!

    self.create_activity key: 'deployment_environment.deploy', owner: user

    DeploymentWorker.perform_async(self.id, self.deployment.application.deployment_module.id)
  end

  def preceding_environment
    self.environment.application.environments.select { |e| e.deployment_order == (self.environment.deployment_order - 1)}.first
  end

  def update_output_log(output)
    self.output_log << output
    self.save
  end

  def self.final_states
    [:deployed, :failed]
  end

  def build_deployment_hash
    {
      application: {
        name: self.deployment.application.name,
        deployment_parameters: self.deployment.application.deployment_parameters
      },
      artifacts: {},
      deployment_environment: {
        deployer: {
          fname: self.deployer.fname,
          lname: self.deployer.lname,
          email: self.deployer.email
        },
        deployer_comment: self.deployer_comment,
        deployment: {
          user: {
            fname: self.deployment.user.fname,
            lname: self.deployment.user.lname,
            email: self.deployment.user.email
          },
          deployment_parameters: self.deployment.deployment_parameters,
        },
        deployment_module: {
          name:     self.deployment.application.deployment_module.name,
          settings: self.deployment.application.deployment_module.settings
        },
        environment: {
            name:                   self.environment.name,
            deployment_parameters:  self.environment.deployment_parameters
        },
      },
    }
  end

  def output_log_to_file(temp_dir, file_name = "#{self.environment.name}_#{self.id}_#{Time.now.strftime('%m-%d-%Y_%H-%M-%S')}_output_log.txt")
    FileUtils::mkdir_p(temp_dir) unless File.exist?(temp_dir)

    file_path = File.join(temp_dir, file_name)

    File.open(file_path, 'w') { |file| file.write(self.output_log.join("\n")) }

    file_path
  end

  def attachment_to_file(temp_dir, file_name = "#{self.environment.name}_#{self.id}_#{Time.now.strftime('%m-%d-%Y_%H-%M-%S')}_output_log.txt")
    output_log_to_file(temp_dir, file_name)
  end

  def ticket
    self.deployment.ticket
  end

  def ticketing_system_permission?(permission)
    self.deployment.ticketable_permission_enabled?(permission) || self.deployment.application.ticketable_permission_enabled?(permission)
  end

  def ticketing_system_permissions?(permissions)
    permission_states = []

    permissions.each do |permission|
      permission_states << (self.deployment.ticketable_permission_enabled?(permission) || self.deployment.application.ticketable_permission_enabled?(permission))
    end

    !permission_states.include?(false)
  end

  def cancel_scheduled_deployment(update_attrs = true)
    if deploy_at_job_id
      old_job = Sidekiq::Queue.new.find_job(self.deploy_at_job_id)

      old_job.delete if old_job
    end

    if update_attrs
      self.update_attribute(:deploy_at, nil)
      self.update_attribute(:deploy_at_job_id, nil)
    end
  end

  def deploy_at_formatted
    self.deploy_at ? self.deploy_at.strftime(I18n.t('general.timestamp_to_str')) : nil
  end

  private
  def set_initial_state
    self.submit!
    self.approve! if self.deployment.application.approval_count == 0
  end

  def setup_approvals
    (0..(self.deployment.application.approval_count-1)).each do
      self.approvals.build
    end
  end

  def setup_output_log
    self.output_log = Array.new
  end

  def schedule_deployment
    if self.deploy_at_changed?
      if self.deploy_at
        # Remove the previously scheduled job
        if self.deploy_at_job_id_was
          self.cancel_scheduled_deployment(false)
        end

        self.schedule!
        self.deploy_at_job_id = DeploymentWorker.perform_in(self.deploy_at, self.id, self.deployment.application.deployment_module_id)
      end
    end
  end
end
