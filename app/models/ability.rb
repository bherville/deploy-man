class Ability
  include CanCan::Ability

  def initialize(user)
    return unless user

    user.roles.each do |role|
      Role.find_by_name(role.name).resource_roles.each do |rr|
        begin
          case rr.resource_type
            when :class, 'class'
              resource = rr.resource_name.camelcase.constantize
            when :symbol, 'symbol'
              resource = rr.resource_name
            else
              resource = rr.resource_name.to_sym
          end

          can rr.permissions, resource
        rescue Exception => e
          Rails.logger.warn(self.class) {e.message}
        end
      end
    end
  end
end
