class Application < ActiveRecord::Base
  include TicketingSystems::ActsAsTicketable
  include PublicActivity::Model

  acts_as_ticketable
  audited
  has_associated_audits
  tracked :owner => proc {|controller, model| controller ? controller.current_user : model.user}


  belongs_to :deployment_module
  belongs_to :user

  has_many :deployments, dependent: :destroy
  has_many :deployment_templates, dependent: :nullify
  has_many :approval_group_applications, dependent: :destroy
  has_many :environments, dependent: :destroy
  has_many :resource_cc_users, as: :ccable, dependent: :destroy
  has_many :watchers, as: :watchable, dependent: :destroy

  has_many :resource_ticketing_system_permissions, as: :resource, dependent: :destroy

  has_many :approval_groups, through: :approval_group_applications
  has_many :cc_users, :through => :resource_cc_users, :source => :user
  has_many :ticketing_system_permissions, through: :resource_ticketing_system_permissions


  accepts_nested_attributes_for :environments, allow_destroy: true, :reject_if => proc { |e| e[:name].blank? }
  accepts_nested_attributes_for :approval_group_applications
  accepts_nested_attributes_for :resource_cc_users
  accepts_nested_attributes_for :resource_ticketing_system_permissions

  validates :name, presence: true, uniqueness: true
  validates :deployment_module_id, presence: true
  validate :valid_environment?

  before_save :mark_environments_for_removal

  serialize :deployment_parameters, Hash

  def approvers
    self.approval_groups.map { |ag| ag.users }.flatten
  end

  def needs_approvals?
    self.approval_count > 0
  end

  def to_s
    self.name
  end

  def mailing_list
    users = []
    users += self.approvers
    users += self.cc_users
    users += self.watchers.map(&:user).flatten
    users <<  self.user unless self.user.nil? || self.approvers.include?(self.user)

    users.flatten.uniq
  end

  private
  def mark_environments_for_removal
    self.environments.each do |environments|
      environments.mark_for_destruction if environments.name.blank? && environments.deployment_order.blank?
    end
  end

  def valid_environment?
    errors.add(:base, 'At least one environment must be created.') unless self.environments.length > 0

    deployment_orders = self.environments.map(&:deployment_order)

    unless deployment_orders.uniq.length == deployment_orders.length
      errors.add(:environments, 'must have deployment order set uniquely.')
    end
  end
end
