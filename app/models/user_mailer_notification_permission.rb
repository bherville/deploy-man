class UserMailerNotificationPermission < ActiveRecord::Base
  audited

  belongs_to :mailer_notification_permission
  belongs_to :user

  validates :mailer_notification_permission_id, presence: true
  validates :user_id, presence: true
  validates_uniqueness_of :user_id, scope: :mailer_notification_permission_id
end
