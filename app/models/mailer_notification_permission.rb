class MailerNotificationPermission < ActiveRecord::Base
  audited

  has_many :user_mailer_notification_permissions, dependent: :delete_all

  validates :name, presence: true
  validates :mailer_name, presence: true
  validates :method_name, presence: true
  validates_uniqueness_of :name, scope: [:mailer_name, :name, :method_name]
end
