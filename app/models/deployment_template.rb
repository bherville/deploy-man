class DeploymentTemplate < ActiveRecord::Base
  audited

  belongs_to :application
  belongs_to :user

  has_many :deployments

  has_many :resource_cc_users, as: :ccable, dependent: :destroy

  has_many :cc_users, :through => :resource_cc_users, :source => :user

  accepts_nested_attributes_for :resource_cc_users

  validates :name, presence: true
  validates_uniqueness_of :name, scope: :user_id

  before_save :parse_deployment_template_parameters

  serialize :deployment_parameters

  def clone_params
    install_attributes = {}

    (self.attribute_names - %w(id created_at updated_at user_id)).each do |a|
      install_attributes[a.to_sym] = self.read_attribute(a)
    end

    install_attributes
  end

  def name_with_user
    "#{self.user.full_name} - #{self.name}"
  end

  private
  def parse_deployment_template_parameters
    if self.deployment_parameters.is_a? String
      parameters = Hash.new

      self.deployment_parameters.split("\r\n").each do |parameter|
        key_value = parameter.split(':')

        parameters[key_value[0]] = key_value[1]
      end

      self.deployment_parameters = parameters
    end
  end
end
