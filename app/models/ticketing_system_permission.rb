class TicketingSystemPermission < ActiveRecord::Base
  has_many :resource_ticketing_system_permissions, dependent: :destroy

  has_many :resources, through: :resource_ticketing_system_permissions
end
