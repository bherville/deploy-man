class Role < ActiveRecord::Base
  audited

  has_many :user_roles

  has_many :resource_roles, dependent: :delete_all

  has_many :users, through: :user_roles

  validates :name, presence: true, uniqueness: true
end
