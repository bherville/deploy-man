require 'zip'

class DeploymentModule < ActiveRecord::Base
  audited

  has_many :applications

  validates :name, presence: true, uniqueness: true
  validates :module_version, presence: true

  serialize :module_files, Array

  serialize :settings, Hash
  serialize :default_parameters_applications, Hash
  serialize :default_parameters_environments, Hash
  serialize :default_parameters_deployments, Hash

  before_destroy :remove_module_directory

  def to_s
    self.name
  end

  def self.available_modules
    dir = Rails.root.join(DeploymentModule.module_directory)

    modules = []

    Dir.foreach(dir) do |item|
      next if item == '.' or item == '..'

      begin
        module_yaml = File.join(dir, item, "#{item}.yml")
        module_rb = File.join(dir, item, "#{item}.rb")

        next unless File.exist?(module_yaml) && File.exist?(module_rb)



        module_info = YAML.load_file(module_yaml)

        next unless DeploymentModule.find_by_name(module_info['module_name']).nil?

        modules << DeploymentModule.new(
            name:           module_info['module_name'],
            description:    module_info['module_description'],
            module_path:    module_rb,
            module_version: module_info['module_version'],
        )
      rescue Exception => e
        logger.error(e)
      end
    end

    modules
  end

  def install_parameters
    install_attributes = {}

    (self.attribute_names - %w(id created_at updated_at)).each do |a|
      install_attributes[a.to_sym] = self.read_attribute(a)
    end

    install_attributes
  end

  def self.upload_and_install(module_archive_file)
    deployment_module = DeploymentModule.new(
      module_files: []
    )

    begin
      Zip::File.open(module_archive_file) do |zip_file|
        entry = zip_file.glob('module.yml').first

        if entry
          yml = entry.get_input_stream.read
          module_info = YAML.load(yml)
          module_name = module_info['module_name']

          if module_name
            module_path = File.join(ENV['DEPLOYMENT_MODULE_BASE_DIRECTORY'] || Rails.root.to_s, DeploymentModule.module_directory, "#{SecureRandom.uuid}_#{module_name.downcase}")

            deployment_module.name            = module_info['module_name']
            deployment_module.description     = module_info['module_description']
            deployment_module.module_path     = File.join(module_path, "#{module_name.downcase}.rb")
            deployment_module.module_version  = module_info['module_version']

            deployment_module.default_parameters_applications   = module_info['default_parameters_applications'] || {}
            deployment_module.default_parameters_deployments    = module_info['default_parameters_deployments'] || {}
            deployment_module.default_parameters_environments   = module_info['default_parameters_environments'] || {}
            deployment_module.settings                          = module_info['default_settings'] || {}

            unless DeploymentModule.where('name = ?', module_info['module_name']).any?
              FileUtils::mkdir_p(module_path)
              deployment_module.deploy_files(module_info, zip_file, module_path)
            end
          else
            deployment_module.errors.add(:module_archive_file, 'has a module.yml file containing an invalid or missing module_name')
          end
        else
          deployment_module.errors.add(:module_archive_file, 'does not contain a module.yml file')
        end
      end
    rescue Exception => e
      deployment_module.errors.add(:module_archive_file, 'is invalid or there was an issue installing the Deployment Module. Please notify the administrator.')
      logger.error(e.message)
    end

    deployment_module.remove_module_directory if deployment_module.errors.any?

    deployment_module
  end

  def upload_and_upgrade(module_archive_file)
    begin
      Zip::File.open(module_archive_file) do |zip_file|
        entry = zip_file.glob('module.yml').first

        if entry
          yml = entry.get_input_stream.read
          module_info = YAML.load(yml)
          module_name = module_info['module_name']

          if module_name
            module_path = Pathname.new(self.module_path).dirname

            self.description     = module_info['module_description']
            self.module_path     = File.join(module_path, "#{module_name.downcase}.rb")
            self.module_version  = module_info['module_version']


            #self.remove_module_directory(false)
            self.deploy_files(module_info, zip_file, module_path)
          else
            self.errors.add(:module_archive_file, 'has a module.yml file containing an invalid or missing module_name')
          end
        else
          self.errors.add(:module_archive_file, 'does not contain a module.yml file')
        end
      end
    rescue Exception => e
      self.errors.add(:module_archive_file, 'is invalid or there was an issue upgrading the Deployment Module. Please notify the administrator.')
      logger.error(e.message)
    end

    self
  end

  def self.module_directory
    AppConfig.get(%w(deployment modules_directory), APP_CONFIG) || File.join('vendor', 'deployment_modules')
  end

  def remove_module_directory(remove_directory = true)
    if self.module_files

      self.module_files.each do |file|
        File.delete(file) if File.exist?(file)
      end

      if remove_directory && self.module_path
        module_dir = File.dirname(self.module_path)
        FileUtils.remove_dir(module_dir) if File.exist?(module_dir)
      end
    else
      logger.warn("The module #{self.id} - #{self.name} did not have the module_files attribute set, not attempting to delete its files.")
    end
  end

  def deploy_files(module_info, zip_file, module_path)
    module_name = module_info['module_name']

    valid_module = {
        'module.yml'                  =>  false,
        "#{module_name.downcase}.rb"  =>  false
    }

    zip_file.each do |entry|
      valid_module[entry.to_s] = true if valid_module.keys.include?(entry.to_s)
      module_file_path = File.join(module_path, entry.to_s)

      File.open(module_file_path, 'w') { |file| file.write(entry.get_input_stream.read) }
      self.module_files << module_file_path
    end

    if valid_module.values.include?(false)
      self.errors.add(:module_archive_file, "is missing module file(s): #{(valid_module.select { |k,v| !v }).keys.join(', ')}")
    end
  end
end
