class Deployment < ActiveRecord::Base
  include TicketingSystems::ActsAsTicketable
  include PublicActivity::Model
  include Workflow

  acts_as_ticketable
  audited associated_with: :application
  has_associated_audits

  tracked owner: proc {|controller, model| controller ? controller.current_user : model.user},
          params: {
              summary: proc {|controller, model| controller ? controller.truncate(model.deployer_comment, length: 30) : nil}
          }


  belongs_to :application
  belongs_to :user
  belongs_to :deployment_template

  has_many :deployment_environments, dependent: :destroy

  has_many :resource_ticketing_system_permissions, as: :resource, dependent: :destroy
  has_many :resource_cc_users, as: :ccable, dependent: :destroy
  has_many :watchers, as: :watchable, dependent: :destroy

  has_many :approvals, through: :deployment_environments
  has_many :cc_users, :through => :resource_cc_users, :source => :user
  has_many :environments, through: :deployment_environments
  has_many :ticketing_system_permissions, through: :resource_ticketing_system_permissions

  accepts_nested_attributes_for :resource_cc_users
  accepts_nested_attributes_for :resource_ticketing_system_permissions


  validates :application_id, presence: true
  validates :user_id, presence: true

  validate :has_environments?

  after_create :set_initial_state

  serialize :deployment_parameters, Hash

  workflow do
    state :new do
      event :submit, transitions_to: :submitted
    end
    state :submitted
    on_transition do |from, to, triggering_event, *event_args|
      DeploymentMailer.initialize_notification(self, to)
      TicketingSystemCommentWorker.perform_async(self.id, self.class.name, to) if ticketing_system_enabled?(:jira) && !self.ticket.blank? && self.ticketable_permission_enabled?(:comment) && self.application.ticketable_permission_enabled?("comment_on_deployment_#{to}".to_sym)
    end
  end

  private
  def set_initial_state
    self.submit!
  end

  def has_environments?
    self.errors.add(:base, 'You must select at least one environment') unless self.deployment_environments.size > 0
  end
end
