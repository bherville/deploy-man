json.id deployment_template.id
json.name deployment_template.name
json.comment deployment_template.comment
json.ticket deployment_template.ticket
json.deployment_parameters deployment_template.deployment_parameters
json.public deployment_template.public
json.post_deployment_ticketing_system_user deployment_template.post_deployment_ticketing_system_user
json.user_id deployment_template.user_id
json.created_at deployment_template.created_at
json.updated_at deployment_template.updated_at
json.url api_deployment_template_url(deployment_template)