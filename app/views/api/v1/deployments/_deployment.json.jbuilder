json.id deployment.id
json.name "#{deployment.application.name} - #{deployment.id}"
json.application_id deployment.application.id
json.deployment_template_id deployment.deployment_template_id
json.created_at deployment.created_at
json.updated_at deployment.updated_at
json.url api_application_deployment_url(deployment.application, deployment)