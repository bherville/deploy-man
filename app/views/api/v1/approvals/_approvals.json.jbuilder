json.id approval.id
json.status approval.status
json.approver_comment approval.approver_comment
json.deployment_id approval.deployment.id
json.application_id approval.deployment.application.id
json.created_at approval.created_at
json.updated_at approval.updated_at
json.url api_application_deployment_approval_url(approval.deployment.application, approval.deployment, approval)