json.auditable audit.auditable
json.auditable_type audit.auditable_type
json.action audit.action
json.version audit.version
json.created_at audit.created_at
json.url api_audit_url(audit)
json.audited_changes audit.audited_changes do |audited_change, values|
  json.audited_change audited_change
  json.new_value (values && values.is_a?(Array)) ? values.first : nil
  json.old_value (values && values.is_a?(Array)) ? values.last : nil
end