json.id user.id
json.fname user.fname
json.lname user.lname
json.full_name user.full_name
json.email user.email
json.gravatar_url user.gravatar_url
json.url api_user_url(user)