class AuditsDatatable
  delegate :params, :h, :link_to, to: :@view
  include Rails.application.routes.url_helpers

  def initialize(view)
    @view = view
  end

  def as_json(options = {})
    {
        sEcho: params[:sEcho].to_i,
        iTotalRecords: Audited.audit_class.count,
        iTotalDisplayRecords: Audited.audit_class.count,
        aaData: data
    }
  end

  private

  def data
    audits.map do |audit|
      [
          audit.id,
          link_to(audit.version, ::Rails.application.routes.url_helpers.admin_audit_path(audit)),
          audit.created_at,
          audit.user ? audit.user.full_name_with_email : I18n.t('audited.username.unknown'),
          defined?(audit.auditable.name) ? audit.auditable.name : nil,
          (link_to(audit.auditable_id, audit.auditable) rescue audit.auditable_id),
          audit.auditable_type,
          audit.action
      ]
    end
  end

  def audits
    @audits ||= fetch_audits
  end

  def fetch_audits
    if sort_direction == :asc
      audits = Audited.audit_class.all.sort { |a| a[sort_column] }
    else
      audits = Audited.audit_class.all.sort {|a,b| b[sort_column] <=> a[sort_column]}
    end

    audits = Kaminari.paginate_array(audits).page(page).per(per_page)
    if params[:sSearch].present?
      audits = audits.where('lower(auditable_type) like :search OR lower(action) like :search', search: "%#{params[:sSearch].downcase}%")
    end
    audits
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id version created_at auditable_id auditable_type action]
    columns[params[:iSortCol_0].to_i]
  end

  def sort_direction
    params[:sSortDir_0].downcase == 'desc' ? :desc : :asc
  end
end