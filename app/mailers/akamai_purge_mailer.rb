class AkamaiPurgeMailer < ApplicationMailer
  add_template_helper(AkamaiPurgeMailerHelper)

  def purge_initiated_notification(deployment_environment_id, akamai_purge_json)
    @deployment_environment = DeploymentEnvironment.find(deployment_environment_id)
    @akamai_purge_json = akamai_purge_json

    mailing_list(@deployment_environment).each do |user|
      mail :to => user.email, :subject => "#{t('application.name')}: #{t('mailers.akamai_purge_mailer.purge_initiated_notification.subject') % @deployment_environment.deployment.id}" if mailer_notification_permissions(user, __method__.to_s, self.class.name.underscore)
    end
  end

  def purge_in_progress_notification(deployment_environment_id, akamai_purge_json)
    @deployment_environment = DeploymentEnvironment.find(deployment_environment_id)
    @akamai_purge_json = akamai_purge_json

    mailing_list(@deployment_environment).each do |user|
      mail :to => user.email, :subject => "#{t('application.name')}: #{t('mailers.akamai_purge_mailer.purge_in_progress_notification.subject') % @deployment_environment.deployment.id}" if mailer_notification_permissions(user, __method__.to_s, self.class.name.underscore)
    end
  end

  def purge_completed_notification(deployment_environment_id, akamai_purge_json)
    @deployment_environment = DeploymentEnvironment.find(deployment_environment_id)
    @akamai_purge_json = akamai_purge_json

    mailing_list(@deployment_environment).each do |user|
      mail :to => user.email, :subject => "#{t('application.name')}: #{t('mailers.akamai_purge_mailer.purge_completed_notification.subject') % @deployment_environment.deployment.id}" if mailer_notification_permissions(user, __method__.to_s, self.class.name.underscore)
    end
  end

  private
  def mailing_list(deployment_environment)
    mailing_list = [] + deployment_environment.deployment.application.cc_users + deployment_environment.deployment.cc_users
    mailing_list << deployment_environment.deployment.user unless deployment_environment.deployment.user.nil? || mailing_list.include?(deployment_environment.deployment.user)
    mailing_list += deployment_environment.deployment.watchers.map(&:user).flatten
    mailing_list += deployment_environment.deployment.application.watchers.map(&:user).flatten

    mailing_list
  end
end
