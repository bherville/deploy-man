class ApprovalMailer < ApplicationMailer
  def awaiting_approval_notification(approval_id)
    @approval = Approval.find(approval_id)
    approvers = mailing_list(@approval)

    approvers.each do |approver|
      mail :to => approver.email, :subject => "#{t('application.name')}: #{t('mailers.approval_mailer.waiting_notification.subject') % [@approval.deployment.id, @approval.environment.name]}" if mailer_notification_permissions(approver, __method__.to_s, self.class.name.underscore)
    end
  end

  def approved_notification(approval_id)
    @approval = Approval.find(approval_id)
    approvers = mailing_list(@approval)

    approvers.each do |approver|
      mail :to => approver.email, :subject => "#{t('application.name')}: #{t('mailers.approval_mailer.approve_notification.subject') % [@approval.deployment.id, @approval.environment.name]}" if mailer_notification_permissions(approver, __method__.to_s, self.class.name.underscore)
    end
  end

  def rejected_notification(approval_id)
    @approval = Approval.find(approval_id)
    approvers = mailing_list(@approval)

    approvers.each do |approver|
      mail :to => approver.email, :subject => "#{t('application.name')}: #{t('mailers.approval_mailer.reject_notification.subject') % [@approval.deployment.id, @approval.environment.name]}" if mailer_notification_permissions(approver, __method__.to_s, self.class.name.underscore)
    end
  end

  def self.initialize_notification(approval, state)
    case state
      when :awaiting_approval
        ApprovalMailer.delay.awaiting_approval_notification(approval.id)
      when :approved
        ApprovalMailer.delay.approved_notification(approval.id)
      when :rejected
        ApprovalMailer.delay.rejected_notification(approval.id)
      else
        p state
    end
  end

  private
  def mailing_list(approval)
    approvers = [] + approval.deployment.application.approvers
    approvers <<  approval.deployment.user unless approval.deployment.user.nil? || approvers.include?(approval.deployment.user)
    approvers += approval.deployment.watchers.map(&:user).flatten
    approvers += approval.deployment.application.watchers.map(&:user).flatten

    approvers.uniq
  end
end
