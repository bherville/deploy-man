class AdminMailer < ApplicationMailer
  def new_user_notification(user_id)
    @user = User.find(user_id)

    admin_role = Role.find_by_name(:admin)
    admins = admin_role ? admin_role.users : []

    admins.each do |admin|
      mail :to => admin.email, :subject => "#{t('application.name')}: #{t('mailers.admin_mailer.new_user_notification.subject')}"# if mailer_notification_permissions(user, __method__.to_s, self.class.name.underscore)
    end
  end
end
