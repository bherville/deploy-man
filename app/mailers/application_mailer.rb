class ApplicationMailer < ActionMailer::Base
  default from: (AppConfig.get(%w(notifications from_address), APP_CONFIG) ? AppConfig.get(%w(notifications from_address), APP_CONFIG) : ENV[:FROM_ADDRESS])
  layout 'mailer'


  def mailer_notification_permissions(user, method_name, class_name)
    mnp = MailerNotificationPermission.find_by_mailer_name_and_method_name(class_name, method_name)
    umnp = UserMailerNotificationPermission.find_by_mailer_notification_permission_id_and_user_id(mnp.id, user.id) if mnp

    if umnp
      umnp.send_notification?
    else
      false
    end
  end
end
