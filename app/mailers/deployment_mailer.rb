class DeploymentMailer < ApplicationMailer
  def new_notification(deployment, mailing_list)
    @deployment = deployment

    mailing_list.each do |approver|
      mail :to => approver.email, :subject => "#{t('application.name')}: #{t('mailers.deployment_mailer.new_notification.subject') % @deployment.id}" if mailer_notification_permissions(approver, __method__.to_s, self.class.name.underscore)
    end
  end

  def self.initialize_notification(deployment, state)
    mailing_list = [] + deployment.application.cc_users + deployment.cc_users
    mailing_list += deployment.application.approval_groups.map(&:users).flatten if deployment.application.approval_groups.count > 0
    mailing_list << deployment.user unless deployment.user.nil? || mailing_list.include?(deployment.user)
    mailing_list += deployment.watchers.map(&:user).flatten
    mailing_list += deployment.application.watchers.map(&:user).flatten

    case state
      when :submitted
        DeploymentMailer.delay.new_notification(deployment, mailing_list.uniq)
      else
      end
  end
end
