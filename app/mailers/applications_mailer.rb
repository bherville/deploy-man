class ApplicationsMailer < ApplicationMailer
  def new_notification(application)
    @application = application

    @application.mailing_list.each do |approver|
      mail :to => approver.email, :subject => "#{t('application.name')}: #{t('mailers.applications_mailer.new_notification.subject')}" if mailer_notification_permissions(approver, __method__.to_s, self.class.name.underscore)
    end
  end

  def destroy_notification(application_name, destroy_user, mailing_list)
    @application_name = application_name
    @destroy_user = destroy_user

    @destroyed_at = Time.now

    mailing_list.each do |user|
      mail :to => user.email, :subject => "#{t('application.name')}: #{t('mailers.applications_mailer.destroy_notification.subject')}" if mailer_notification_permissions(user, __method__.to_s, self.class.name.underscore)
    end
  end
end
