class DeploymentEnvironmentsMailer < ApplicationMailer
  def deploy_notification(deployment_environment, mailing_list, state)
    @deployment_environment = deployment_environment
    @state = state

    mailing_list.each do |approver|
      mail :to => approver.email, :subject => "#{t('application.name')}: #{t('mailers.deployment_environments_mailer.deploy_notification.subject') % [@deployment_environment.deployment.id, @deployment_environment.environment.name, @state.to_s.titleize]}" if mailer_notification_permissions(approver, __method__.to_s, self.class.name.underscore)
    end
  end

  def scheduled_notification(deployment_environment, mailing_list, state)
    @deployment_environment = deployment_environment
    @state = state

    mailing_list.each do |approver|
      @user = approver
      mail :to => approver.email, :subject => "#{t('application.name')}: #{t('mailers.deployment_environments_mailer.deploy_notification.subject') % [@deployment_environment.deployment.id, @deployment_environment.environment.name, @state.to_s.titleize]}" if mailer_notification_permissions(approver, __method__.to_s, self.class.name.underscore)
    end
  end

  def deployed_notification(deployment_environment, mailing_list, state)
    @deployment_environment = deployment_environment
    @state = state

    output_log_file_path = @deployment_environment.output_log_to_file(Rails.root.join('tmp'), SecureRandom.uuid)

    attachments["#{@deployment_environment.environment.name}_#{@deployment_environment.id}_#{Time.now.strftime('%m-%d-%Y_%H-%M-%S')}_output_log.txt"] = File.read(output_log_file_path)
    logger.info("Output Log: #{output_log_file_path}")

    mailing_list.each do |approver|
      mail :to => approver.email, :subject => "#{t('application.name')}: #{t('mailers.deployment_environments_mailer.deploy_notification.subject') % [@deployment_environment.deployment.id, @deployment_environment.environment.name, @state.to_s.titleize]}" if mailer_notification_permissions(approver, __method__.to_s, self.class.name.underscore)
    end


    if File.exist?(output_log_file_path)
      File.delete(output_log_file_path)
    else
      logger.warn("Attempted to delete nonexistent output log at #{output_log_file_path}")
    end
  end

  def self.initialize_notification(deployment_environment_id, state)
    deployment_environment = DeploymentEnvironment.find(deployment_environment_id)

    mailing_list = [] + deployment_environment.deployment.application.cc_users + deployment_environment.deployment.cc_users
    mailing_list << deployment_environment.deployment.user unless deployment_environment.deployment.user.nil? || mailing_list.include?(deployment_environment.deployment.user)
    mailing_list += deployment_environment.deployment.watchers.map(&:user).flatten
    mailing_list += deployment_environment.deployment.application.watchers.map(&:user).flatten

    case state
      when :queued, :deploying
        DeploymentEnvironmentsMailer.delay.deploy_notification(deployment_environment, mailing_list, state)
      when :scheduled
        DeploymentEnvironmentsMailer.delay.scheduled_notification(deployment_environment, mailing_list, state)
      when :deployed, :failed
        DeploymentEnvironmentsMailer.delay.deployed_notification(deployment_environment, mailing_list, state)
      else
    end
  end
end
