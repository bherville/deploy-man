class TicketingSystemAttachmentWorker
  include Sidekiq::Worker
  include TicketingSystems::Jira

  def perform(resource_id, resource_class, state, state_comment = false, assign_to_user = nil)
    state = state.to_s

    resource = resource_class.constantize.find(resource_id)
    attachment = resource.attachment_to_file(Rails.root.join('tmp', "#{SecureRandom.uuid}"))

    TicketingSystems::Jira.add_comment(resource.deployment.ticket, build_status_comment(resource, state)) if state_comment

    TicketingSystems::Jira.attach_file(resource.deployment.ticket, attachment)
    TicketingSystems::Jira.add_comment(resource.deployment.ticket, build_attachment_comment(resource, attachment))

    if assign_to_user && !assign_to_user.blank?
      TicketingSystems::Jira.add_comment(resource.deployment.ticket, build_assignment_comment(resource, assign_to_user))
      TicketingSystems::Jira.assign_issue(resource.deployment.ticket, assign_to_user)
    end

    if File.exist?(attachment)
      File.delete(attachment)
    else
      logger.warn("Attempted to delete nonexistent JiraAttachmentFile at #{attachment}")
    end

    if File.exist?(File.dirname(attachment))
      FileUtils.remove_dir(File.dirname(attachment))
    else
      logger.warn("Attempted to delete nonexistent JiraAttachmentFile at #{File.dirname(attachment)}")
    end
  end

  private
  def build_status_comment(resource, state)
    view = html = ActionView::Base.new(Rails.root.join('app/views'))
    view.class.include ApplicationHelper

    view.render(
        partial: "ticketing_systems/jira/#{resource.class.name.underscore.pluralize}/#{state}",
        locals: {
            resource: resource,
            state:    state
        },
        layout: 'ticketing_systems/jira/comment'
    )
  end

  def build_attachment_comment(resource, attachment)
    view = html = ActionView::Base.new(Rails.root.join('app/views'))
    view.class.include ApplicationHelper

    view.render(
        partial: "ticketing_systems/jira/#{resource.class.name.underscore.pluralize}/attachment",
        locals: {
            resource:   resource,
            state:      'Deployment Log Attached',
            attachment: attachment
        },
        layout: 'ticketing_systems/jira/comment'
    )
  end

  def build_assignment_comment(resource, assign_to_user)
    view = html = ActionView::Base.new(Rails.root.join('app/views'))
    view.class.include ApplicationHelper

    view.render(
        partial: "ticketing_systems/jira/#{resource.class.name.underscore.pluralize}/assign",
        locals: {
            resource:   resource,
            state:      'Reassigning Ticket',
            assign_to_user: assign_to_user
        },
        layout: 'ticketing_systems/jira/comment'
    )
  end
end
