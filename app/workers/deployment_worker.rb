class DeploymentWorker
  include Sidekiq::Worker
  sidekiq_options :retry => false

  def perform(deployment_environment_id, deployment_module_id)
    deployment_module = DeploymentModule.find(deployment_module_id)
    deployment_environment = DeploymentEnvironment.find(deployment_environment_id)

    require Rails.root.join(deployment_module.module_path)


    deployment_class_constant = deployment_module.name.to_s.titleize.constantize


    deployment_class_object = deployment_class_constant.new(deployment_environment_id)

    deployment_environment.process!


    # Perform Deployment
    deployment_hash = deployment_environment.build_deployment_hash
    exception_thrown = nil

    begin
      deployment_hash[:artifacts][:pre_deploy] = deployment_class_object.pre_deploy(deployment_hash) if defined?(deployment_class_object.pre_deploy)
      deployment_hash[:artifacts][:pre_deploy] ||= {}

      deployment_hash[:artifacts][:deploy] = deployment_class_object.deploy(deployment_hash) if defined?(deployment_class_object.deploy)
      deployment_hash[:artifacts][:deploy] ||= {}

      deployment_hash[:artifacts][:post_deploy] = deployment_class_object.post_deploy(deployment_hash) if defined?(deployment_class_object.post_deploy)
      deployment_hash[:artifacts][:post_deploy] ||= {}



      # Process deploy results
      successes = {}

      successes[:pre_deploy] = deployment_hash[:artifacts][:pre_deploy][:success] if valid_return_hash(deployment_hash[:artifacts], :pre_deploy)
      successes[:deploy] = deployment_hash[:artifacts][:deploy][:success] if valid_return_hash(deployment_hash[:artifacts], :deploy)
      successes[:post_deploy] = deployment_hash[:artifacts][:post_deploy][:success] if valid_return_hash(deployment_hash[:artifacts], :post_deploy)
    rescue Exception => e
      deployment_environment.update_output_log(e)

      exception_thrown = e

      logger.fatal(e.message)
    end


    if exception_thrown || successes.values.include?(false)
      deployment_environment.deploy_failed!
    else
      deployment_environment.deploy_succeeded!

      deployment_environment.clear_akamai_cache! if (AppConfig.get(%W(akamai_purge enable), APP_CONFIG) && deployment_environment.environment.akamai_cp_codes && deployment_environment.deployment.akamai_cache_purge? && deployment_environment.environment.akamai_cp_codes.count > 0)
    end

    #Object.send(:remove_const,  deployment_module.name.to_s.titleize.to_sym)

    raise exception_thrown if exception_thrown
  end

  private
  def valid_return_hash(artifacts_hash, stage)
    # If the artifact_hash[stage] is a Hash and the artifact_hash[stage][success] is a Boolean
    artifacts_hash[stage].is_a?(Hash) && (!!artifacts_hash[stage][:success] == artifacts_hash[stage][:success])
  end
end

class DeploymentOutputLogListener
  def self.update_output_log(deployment_environment_id, message)
    DeploymentEnvironment.find(deployment_environment_id).update_output_log(message)
  end
end