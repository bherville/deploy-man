class AkamaiPurgeStatusWorker
  include Sidekiq::Worker

  def perform(deployment_environment_id, purge_id)
    if AppConfig.get(%W(akamai_purge akamai_username), APP_CONFIG) && AppConfig.get(%W(akamai_purge akamai_password), APP_CONFIG)
      client = AkamaiPurge::Client.new(AppConfig.get(%W(akamai_purge akamai_username), APP_CONFIG), AppConfig.get(%W(akamai_purge akamai_password), APP_CONFIG))

      deployment_environment = DeploymentEnvironment.find(deployment_environment_id)

      if deployment_environment
        akamai_purge_json = client.status(purge_id)

        logger.info("Akamai Cache Purge #{akamai_purge_json['purgeId']}: #{akamai_purge_json['purgeStatus']}")

        case akamai_purge_json['purgeStatus']
          when 'In-Progress'
            AkamaiPurgeMailer.delay.purge_in_progress_notification(deployment_environment_id, akamai_purge_json)

            AkamaiPurgeStatusWorker.perform_in((AppConfig.get(%W(akamai_purge akamai_purge_status_check_interval), APP_CONFIG) || akamai_purge_json['pingAfterSeconds']).to_i.seconds, deployment_environment_id, akamai_purge_json['purgeId'])
          when 'Done'
            AkamaiPurgeMailer.delay.purge_completed_notification(deployment_environment_id, akamai_purge_json)
            deployment_environment.akamai_cache_cleared!
          else
            logger.warn("Akamai Purge Status Unknown state: #{akamai_purge_json['purgeStatus']}")
            logger.warn(akamai_purge_json)
            deployment_environment.akamai_cache_cleared!
        end
      end
    else
      logger.warn('Akamai Purge akamai_username or akamai_password missing from app config.')
    end
  end
end
