class TicketingSystemCommentWorker
  include Sidekiq::Worker
  include TicketingSystems::Jira

  def perform(resource_id, resource_class, state)
    state = state.to_s

    begin
      resource = resource_class.constantize.find(resource_id)

      if resource
        view = html = ActionView::Base.new(Rails.root.join('app/views'))
        view.class.include ApplicationHelper
        comment = view.render(
            partial: "ticketing_systems/jira/#{resource_class.underscore.pluralize}/#{state}",
            locals: {
                resource: resource,
                state: state
            },
            layout: 'ticketing_systems/jira/comment'
        )

        TicketingSystems::Jira.add_comment(resource.ticket, comment)
      end
    rescue Exception => e
      logger.error(e.message)
    end
  end
end
