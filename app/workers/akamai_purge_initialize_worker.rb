class AkamaiPurgeInitializeWorker
  include Sidekiq::Worker

  def perform(deployment_environment_id, ping_after_seconds = nil)
    if AppConfig.get(%W(akamai_purge akamai_username), APP_CONFIG) && AppConfig.get(%W(akamai_purge akamai_password), APP_CONFIG)
      client = AkamaiPurge::Client.new(AppConfig.get(%W(akamai_purge akamai_username), APP_CONFIG), AppConfig.get(%W(akamai_purge akamai_password), APP_CONFIG))

      deployment_environment = DeploymentEnvironment.find(deployment_environment_id)

      if deployment_environment
        akamai_purge_json = client.purge(deployment_environment.environment.akamai_cp_codes)

        AkamaiPurgeMailer.delay.purge_initiated_notification(deployment_environment_id, akamai_purge_json)

        AkamaiPurgeStatusWorker.perform_in((ping_after_seconds || akamai_purge_json['pingAfterSeconds']).to_i.seconds, deployment_environment_id, akamai_purge_json['purgeId'])
      end
    else
      logger.warn('Akamai Purge akamai_username or akamai_password missing from app config.')
    end
  end
end
