class API::V1::DeploymentTemplatesController < ApplicationController
  load_and_authorize_resource
  before_action :set_deployment_template, only: [:show]
  before_action :can_access?, only: [:show]

  swagger_controller :deployment_templates, 'Deployment Template Management'

  swagger_api :index do
    summary 'Fetches all Deployment Template items'
    notes 'This lists all the Deployment Templates'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
  end

  swagger_api :show do
    summary 'Fetches a single Deployment Template item'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment Template ID'
  end


  # GET /api/deployment_templates
  def index
    @deployment_templates = (DeploymentTemplate.select { |dt| dt.public } + current_user.deployment_templates).uniq
  end

  # GET /api/deployment_templates/1
  def show
  end

  private
  def can_access?
    @deployment_template.user == current_user || @deployment_template.public?
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_deployment_template
    @deployment_template = DeploymentTemplate.find(params[:id])
  end
end
