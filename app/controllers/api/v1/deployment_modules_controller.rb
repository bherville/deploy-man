class API::V1::DeploymentModulesController < ApplicationController
  load_and_authorize_resource
  before_action :set_deployment_module, only: [:show]

  swagger_controller :deployment_modules, 'Deployment Module Management'

  swagger_api :index do
    summary 'Fetches all Deployment Module items'
    notes 'This lists all the Deployment Modules'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
  end

  swagger_api :show do
    summary 'Fetches a single Deployment Module item'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment Module ID'
  end


  # GET /api/deployment_modules
  def index
    @deployment_modules = DeploymentModule.all
  end

  # GET /api/deployment_modules/1
  def show
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_deployment_module
    @deployment_module = DeploymentModule.find(params[:id])
  end
end
