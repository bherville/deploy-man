class API::V1::DeploymentsController < API::BaseController
  load_and_authorize_resource
  before_action :set_deployment, except: [:index]
  before_action :set_application
  before_action :approved?, only: [:deploy]
  before_action :deploy?, only: [:deploy]

  swagger_controller :deployments, 'Deployment Management'

  swagger_api :index do
    summary 'Fetches all Deployment items'
    notes 'This lists all the Deployments'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :show do
    summary 'Fetches a single Deployment item'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :create do
    summary 'Create a Deployment item'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :application_id, :integer, :required, 'Application ID'
    param :form, 'deployment[environment_ids]', :array, :optional, 'Environments to deploy to'
    param :form, 'deployment[comment]', :string, :optional, 'Deployment Comment'
    param :form, 'deployment[cc_user_ids]', :string, :optional, "CC'd Users"
    param :form, 'deployment[deployment_parameters]', :string, :optional, 'Deployment Parameters'
    param :form, 'deployment[ticket]', :string, :optional, 'Ticketing System Ticket ID'
    param :form, 'deployment[post_deployment_ticketing_system_user]', :string, :optional, 'The user to auto-reassign tickets to in the ticketing system'
    param :form, 'deployment[resource_ticketing_system_permissions_attributes]', :string, :optional, 'Ticketing system permissions'
    response :unauthorized
    response :not_acceptable
    response :unprocessable_entity
  end

  swagger_api :update do
    summary 'Update a Deployment item'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
    param :form, 'deployment[environment_ids]', :array, :optional, 'Environments to deploy to'
    param :form, 'deployment[comment]', :string, :optional, 'Deployment Comment'
    param :form, 'deployment[cc_user_ids]', :string, :optional, "CC'd Users"
    param :form, 'deployment[deployment_parameters]', :string, :optional, 'Deployment Parameters'
    param :form, 'deployment[ticket]', :string, :optional, 'Ticketing System Ticket ID'
    param :form, 'deployment[post_deployment_ticketing_system_user]', :string, :optional, 'The user to auto-reassign tickets to in the ticketing system'
    param :form, 'deployment[resource_ticketing_system_permissions_attributes]', :string, :optional, 'Ticketing system permissions'
    response :unauthorized
    response :not_acceptable
    response :unprocessable_entity
  end

  swagger_api :deploy do
    summary 'Deploy the Deployment'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :watch do
    summary 'Adds the accessing user to the watchers list for the Deployment'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :unwatch do
    summary 'Removes the accessing user from the watchers list for the Deployment'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  # GET /api/applications/:application_id/deployments.json
  def index
    @deployments = @application.deployments
  end

  # GET /api/applications/:application_id/deployments/:id.json
  def show
  end

  # POST /api/applications/:application_id/deployments.json
  def create
    @deployment = @application.deployments.new(deployment_params)
    @deployment.user = current_user

    respond_to do |format|
      if @deployment.save
        format.json { render json: @deployment, status: :created, location: api_application_deployment_path(@application, @deployment) }
      else
        format.json { render json: @deployment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /api/applications/:application_id/deployments/:id.json
  def update
    respond_to do |format|
      if @deployment.update(deployment_params)
        format.json { head :no_content }
      else
        format.json { render json: @deployment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT/PATCH /api/applications/:application_id/deployments/:id.json
  def deploy
    respond_to do |format|
      if @deployment.deploy(current_user, ActionController::Base.helpers.sanitize(params[:deployer_comment]))
        format.json { render json: @deployment, status: :created, location: api_application_deployment_path(@application, @deployment) }
      else
        format.json { render json: @deployment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /api/applications/:application_id/deployments/:id/watch.json
  def watch
    @deployment.watchers.new(user: current_user)

    respond_to do |format|
      if @deployment.save
        format.json { render partial: 'watch', status: :created, location: application_deployment_path(@deployment.application, @deployment) }
      else
        format.json { render json: @deployment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /api/applications/:application_id/deployments/:id/unwatch.json
  def unwatch
    respond_to do |format|
      if @deployment.watchers.find_by_user_id(current_user.id).destroy
        format.json { render partial: 'unwatch', status: 200 }
      else
        format.json { render json: @deployment.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_deployment
    @deployment = Deployment.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_application
    @application = Application.find(params[:application_id])
  end

  def approved?
    unless @deployment.approved?
      respond_to do |format|
        format.json { render json: APIError::Exceptions.forbidden(Exception.new("You are not allowed to perform the '#{params[:action]}' action on this '#{params[:controller].split('/').last.singularize.titleize}' as its not approved")), status: :unprocessable_entity }
      end
    end
  end

  def deploy?
    unless @deployment.can_deploy? || params[:force] == true
      respond_to do |format|
        format.json { render json: APIError::Exceptions.forbidden(Exception.new("You are not allowed to perform the '#{params[:action]}' action on this '#{params[:controller].split('/').last.singularize.titleize}' as its #{@deployment.status}")), status: :unprocessable_entity }
      end
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def deployment_params
    deployment_parameters = (params[:deployment] && params[:deployment][:deployment_parameters]) ? (YAML.load(params[:deployment][:deployment_parameters]) || @deployment.deployment_parameters) : @deployment.deployment_parameters

    params.require(:deployment).permit(:comment, :ticket, :deployment_template_id, :post_deployment_ticketing_system_user, cc_user_ids: [], environment_ids: [], resource_ticketing_system_permissions_attributes: [:id, :enabled, :ticketing_system_permission_id]).merge(deployment_parameters: deployment_parameters)
  end
end