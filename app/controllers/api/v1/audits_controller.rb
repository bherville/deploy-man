class API::V1::AuditsController < API::BaseController
  authorize_resource :class => false

  swagger_controller :audits, 'Audit Management'

  swagger_api :index do
    summary 'Fetches all Audit items'
    notes 'This lists all the Audits'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :query, :page, :integer, :optional, 'Page of Audits to retrieve'
    param :query, :per, :integer, :optional, 'Audits per page to show'
    param :query, :datatable, :boolean, :optional, 'Returns JSON in appropriate format for JQuery DataTables'
  end

  # GET /api/audits.json
  def index
    if params[:datatable]
      respond_to do |format|
        format.json { render json: AuditsDatatable.new(view_context) }
      end
    else
      per = params[:per] || 10

      @audits = Audited.audit_class.page(params[:page]).per(per)
    end
  end
end
