class API::V1::EnvironmentsController < ApplicationController
  load_and_authorize_resource
  before_action :set_application

  swagger_controller :environments, 'Environment Management'

  swagger_api :index do
    summary 'Fetches all Environment items'
    notes 'This lists all the Environments'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
  end

  swagger_api :show do
    summary 'Fetches a single Environment item'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Environment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :destroy do
    summary 'Destroy the Environment'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Environment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :deploy do
    summary 'Deploy the to the Environment'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Environment ID'
    param :path, :deployment_id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  # GET /api/applications/:application_id/environments.json
  def index
    @environments = @application.environments
  end

  # GET /api/applications/:application_id/environments/:id.json
  def show
  end

  # DELETE /api/applications/:application_id/environments/:id.json
  def destroy
    respond_to do |format|
      if @environment.destroy
        format.json { head :no_content }
      else
        format.json { render json: @environment.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_environment
    @environment = Environment.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_application
    @application = Application.find(params[:application_id])
  end
end
