class API::V1::UsersController < ApplicationController
  load_and_authorize_resource

  swagger_controller :users, 'User Management'

  swagger_api :index do
    summary 'Fetches all User items'
    notes 'This lists all the Users'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :query, :q, :string, :optional, 'Search Term'
    param :query, :case_insensitive, :boolean, :optional, 'Case insensitivity'
  end

  swagger_api :index do
    summary 'Fetches a single User item'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'User iD'
  end

  # GET /api/users.json
  def index
    if params[:q]
      search_param = params[:q].split(' ')

      if params[:case_insensitive] && (params[:case_insensitive].downcase == 'true' || params[:case_insensitive] == '1')
        @users = User.where('lower(fname) LIKE ? OR lower(lname) LIKE ?', search_param.first, search_param.last)
        if @users.count > 0
          p @users
        end
      else
        @users = User.where('fname LIKE ? OR lname LIKE ?', search_param.first, search_param.last)
      end
    else
      @users = User.all
    end
  end

  # GET /api/users/:id.json
  def show
    @users = User.find(params[:id])
  end
end
