class API::V1::ApplicationsController < ApplicationController
  load_and_authorize_resource
  before_action :set_application, only: [:watch, :unwatch]

  swagger_controller :applications, 'Application Management'

  swagger_api :watch do
    summary 'Adds the accessing user to the watchers list for the Application'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Application ID'
  end

  swagger_api :unwatch do
    summary 'Removes the accessing user from the watchers list for the Application'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Application ID'
  end


  # PUT /api/applications/:id/watch.json
  def watch
    @application.watchers.new(user: current_user)

    respond_to do |format|
      if @application.save
        format.json { render partial: 'watch', status: :created, location: application_path(@application) }
      else
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /api/applications/:id/unwatch.json
  def unwatch
    respond_to do |format|
      if @application.watchers.find_by_user_id(current_user.id).destroy
        format.json { render partial: 'unwatch', status: 200 }
      else
        format.json { render json: @application.errors, status: :unprocessable_entity }
      end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_application
    @application = Application.find(params[:id])
  end
end
