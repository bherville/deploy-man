class API::V1::ApprovalsController < API::BaseController
  load_and_authorize_resource
  before_action :set_approval, except: [:index, :waiting]
  before_action :set_deployment, :except => [:waiting]
  before_action :set_deployment_environment, :except => [:waiting]
  before_action :set_application, :except => [:waiting]
  before_action :can_approve_reject?, only: [:approve, :reject]
  before_action :already_approved_rejected?, only: [:approve, :reject]
  after_action :approve_reject_deployment, only: [:approve, :reject]

  swagger_controller :approvals, 'Approval Management'

  swagger_api :index do
    summary 'Fetches all Approval items'
    notes 'This lists all the Approvals'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
  end

  swagger_api :show do
    summary 'Fetches a single Approval item'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Approval ID'
    param :path, :deployment_environment_id, :integer, :required, 'Deployment Environment ID'
    param :path, :deployment_id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :approve do
    summary 'Approve the Approval'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Approval ID'
    param :path, :deployment_environment_id, :integer, :required, 'Deployment Environment ID'
    param :path, :deployment_id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :reject do
    summary 'Reject the Approval'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Approval ID'
    param :path, :deployment_environment_id, :integer, :required, 'Deployment Environment ID'
    param :path, :deployment_id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :waiting do
    summary 'Approvals waiting'
    param :header, 'Authentication-Token', :string, :required, 'Authentication token'
  end

  # GET /api/applications/:application_id/deployments/:deployment_id/deployment_environments/:deployment_environment_id/approvals.json
  def index
    @approvals = @deployment.approvals
  end

  # GET /api/applications/:application_id/deployments/:deployment_id/deployment_environments/:deployment_environment_id/approvals/:id.json
  def show
  end

  # PUT/PATCH /api/applications/:application_id/deployments/:deployment_id/deployment_environments/:deployment_environment_id/approvals/:id/approve.json
  def approve
     respond_to do |format|
      if @approval.approve!(current_user, ActionController::Base.helpers.sanitize(params[:approver_comment]))
        format.json { render json: @approval, status: :created, location: api_application_deployment_deployment_environment_approval_path(@application, @deployment, @deployment_environment, @approval) }
      else
        format.json { render json: @approval.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT/PATCH /api/applications/:application_id/deployments/:deployment_id/deployment_environments/:deployment_environment_id/approvals/:id/reject.json
  def reject
    respond_to do |format|
      if @approval.reject!(current_user, ActionController::Base.helpers.sanitize(params[:approver_comment]))
        format.json { render json: @approval, status: :created, location: api_application_deployment_deployment_environment_approval_path(@application, @deployment, @deployment_environment, @approval) }
      else
        format.json { render json: @approval.errors, status: :unprocessable_entity }
      end
    end
  end

  def waiting
    @approvals = Approval.approvals_for_user(current_user)
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_approval
    @approval = Approval.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_application
    @application = Application.find(params[:application_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_deployment
    @deployment = Deployment.find(params[:deployment_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_deployment_environment
    @deployment_environment = DeploymentEnvironment.find(params[:deployment_environment_id])
  end

  def can_approve_reject?
    unless @approval.can_approve_reject? current_user
      respond_to do |format|
        format.json { render json: APIError::Exceptions.forbidden(Exception.new("You are not allowed to perform the '#{params[:action]}' action on this '#{params[:controller].split('/').last.singularize.titleize}'")), status: :unprocessable_entity }
      end
    end
  end

  def already_approved_rejected?
    if @approval.already_approved_rejected? && !current_user.has_role?(:approval_admin)
      respond_to do |format|
        format.json { render json: APIError::Exceptions.forbidden(Exception.new("You are not allowed to perform the '#{params[:action]}' action on this '#{params[:controller].split('/').last.singularize.titleize}' as its already #{@approval.current_state.to_s}")), status: :unprocessable_entity }
      end
    else
      already = false

      case params[:action].to_sym
        when :approve
          already = @approval.approved?
        when :reject
          already = @approval.rejected?
      end

      if already
        respond_to do |format|
          format.json { render json: APIError::Exceptions.forbidden(Exception.new("You are not allowed to perform the '#{params[:action]}' action on this '#{params[:controller].split('/').last.singularize.titleize}' as its already #{@approval.current_state.to_s}")), status: :unprocessable_entity }
        end
      end
    end
  end

  def approve_reject_deployment
    if @deployment_environment.approved?
      @deployment_environment.approve!
    elsif @deployment_environment.rejected?
      @deployment_environment.reject!
    end
  end
end