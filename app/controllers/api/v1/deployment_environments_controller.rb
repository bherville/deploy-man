class API::V1::DeploymentEnvironmentsController < ApplicationController
  include ActionController::Live

  load_and_authorize_resource
  before_action :set_application
  before_action :set_deployment_environment, only: [:deploy, :log_output, :cancel_schedule, :update_schedule]
  before_action :approved?, only: [:deploy]
  before_action :deploy?, only: [:deploy]
  before_action :previous_deploy?, only: [:deploy]

  swagger_controller :deployment_environments, 'Deployment Environment Management'

  swagger_api :deploy do
    summary 'Deploy the to the Environment'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment Environment ID'
    param :path, :deployment_id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
  end

  swagger_api :log_output do
    summary 'Log output from deployment'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :path, :id, :integer, :required, 'Deployment Environment ID'
    param :path, :deployment_id, :integer, :required, 'Deployment ID'
    param :path, :application_id, :integer, :required, 'Application ID'
    param :form, :output, :string, :required, 'Output to log'
  end

  # PATCH/PUT /applications/:application_id/deployments/:deployment_id/deployment_environments/:id
  def update_schedule
    @deployment_environment.deployer = current_user

    respond_to do |format|
      if @deployment_environment.update(deployment_environment_params)
        format.json { render json: @deployment_environment, status: 200, location: api_application_deployment_path(@application, @deployment_environment.deployment) }
      else
        format.json { render json: @deployment_environment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /applications/1/cancel_schedule
  def cancel_schedule
    @deployment_environment.cancel_scheduled_deployment
    @deployment_environment.cancel_schedule!

    respond_to do |format|
      format.json { render json: @deployment_environment, status: 200, location: api_application_deployment_path(@application, @deployment_environment.deployment) }
    end
  end

  # POST /api/applications/:application_id/deployments/:Deployment_id/deployment_environments/:deployment_environment_id/deploy.json
  def deploy
    respond_to do |format|
      if @deployment_environment.deploy(current_user, ActionController::Base.helpers.sanitize(params[:deployer_comment]))
        format.json { render json: @deployment_environment, status: 200, location: api_application_deployment_path(@application, @deployment_environment.deployment) }
      else
        format.json { render json: @deployment_environment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT/PATCH /api/applications/:application_id/deployments/:deployment_id/deployment_environments/:deployment_environment_id/log_output.json
  def log_output
    @deployment_environment.log_output(params[:output])
  end

  def output
    @output = @deployment_environment.log_output

    render stream: true
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_deployment_environment
    @deployment_environment = DeploymentEnvironment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def deployment_environment_params
    params.require(:deployment_environment).permit(:deploy_at)
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_application
    @application = Application.find(params[:application_id])
  end

  def approved?
    unless @deployment_environment.approved?
      respond_to do |format|
        format.json { render json: APIError::Exceptions.forbidden(Exception.new("You are not allowed to perform the '#{params[:action]}' action on this '#{params[:controller].split('/').last.singularize.titleize}' as its not approved")), status: :unprocessable_entity }
      end
    end
  end

  def deploy?
    unless @deployment_environment.approved? || params[:force] == true
      respond_to do |format|
        format.json { render json: APIError::Exceptions.forbidden(Exception.new("You are not allowed to perform the '#{params[:action]}' action on this '#{params[:controller].split('/').last.singularize.titleize}' as its #{@deployment_environment.deployment_status}")), status: :unprocessable_entity }
      end
    end
  end

  def previous_deploy?
    unless @deployment_environment.approved? && (@deployment_environment.preceding_environment.nil? || DeploymentEnvironment.find_by_deployment_id_and_environment_id(@deployment_environment.deployment.id, @deployment_environment.preceding_environment.id).deployed?)
      respond_to do |format|
        format.json { render json: APIError::Exceptions.forbidden(Exception.new("You are not allowed to perform the '#{params[:action]}' action on this '#{params[:controller].split('/').last.singularize.titleize}' as the preceding environment #{@deployment_environment.preceding_environment.name} has not yet been deployed to.")), status: :unprocessable_entity }
      end
    end
  end
end
