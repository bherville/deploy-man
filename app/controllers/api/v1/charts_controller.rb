class API::V1::ChartsController < ApplicationController
  authorize_resource :class => false

  swagger_controller :audits, 'Chart Data'

  swagger_api :approvals_per_deployment do
    summary 'Fetches approvals per deployments'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
  end

  swagger_api :deployments_per_application do
    summary 'Fetches deployments per application'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
  end

  swagger_api :deployments_created_last_x_days do
    summary 'Fetches deployments created in the last X days'
    param :header, 'X-User-Email', :string, :required, 'User Email'
    param :header, 'X-User-Token', :string, :required, 'User Token'
    param :query, :ago, :integer, :optional, 'The days ago to fetch deployments'
  end

  def approvals_per_deployment
    @approvals = Approval.all
    if @approvals.count > 1
      render json: Approval.all.joins(:environment).where(workflow_state: :awaiting_approval).joins('JOIN applications ON applications.id = application_id').group('applications.name').count
    else
      render json: {}
    end
  end

  def deployments_per_application
    render json: Deployment.all.joins(:application).group(:name).count
  end

  def deployments_created_last_x_days
    start = 15

    if params[:ago] && (Float(params[:ago]) rescue false)
      start = params[:ago].to_i
    end

    render json: DeploymentEnvironment.group_by_day(:created_at, format: '%b %-d').where('created_at > ?', start.days.ago).count
  end
end
