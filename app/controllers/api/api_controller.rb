class API::APIController < ApplicationController
  authorize_resource :class => false

  def show
    versions = ApiConstraints.versions

    api_versions = {
        :api => versions,
    }

    respond_to do |format|
      format.html
      format.json { render json: api_versions }
    end
  end
end