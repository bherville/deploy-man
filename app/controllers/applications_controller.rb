class ApplicationsController < ApplicationController
  load_and_authorize_resource

  before_action :set_application, only: [:show, :edit, :update, :destroy]
  before_action :set_cc_users, only: [:edit, :update]

  # GET /applications
  def index
    @applications = Application.all
  end

  # GET /applications/1
  def show
  end

  # GET /applications/new
  def new
    @application = Application.new
    @application.environments.build

    @application.add_ticketable_permissions

    @deployment_parameters = @application.deployment_parameters.to_yaml
  end

  # GET /applications/1/edit
  def edit
    @deployment_parameters = deployment_parameters_to_string(@application.deployment_parameters) || params[:deployment_parameters]
    @application.environments.build
  end

  # POST /applications
  def create
    @application = current_user.applications.new(application_params)

    respond_to do |format|
      if @application.save
        ApplicationsMailer.delay.new_notification(@application)
        format.html { redirect_to @application, notice: 'Application was successfully created.' }
      else
        @deployment_parameters = deployment_parameters_to_string(@application.deployment_parameters) || params[:deployment_parameters]
        @application.environments.build
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /applications/1
  def update
    respond_to do |format|
      if @application.update(application_params)
        format.html { redirect_to @application, notice: 'Application was successfully updated.' }
      else
        @deployment_parameters = deployment_parameters_to_string(@application.deployment_parameters) || params[:deployment_parameters]
        format.html { render :edit }
      end
    end
  end

  # DELETE /applications/1
  def destroy
    application_name = @application.name
    destroy_user = current_user
    mailing_list = @application.mailing_list

    @application.destroy

    ApplicationsMailer.delay.destroy_notification(application_name, destroy_user, mailing_list)

    respond_to do |format|
      format.html { redirect_to applications_url, notice: 'Application was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_application
      @application = Application.find(params[:id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_cc_users
      @cc_users = @application.cc_users
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def application_params
      safe_param = params.require(:application).permit!#(:name, :deployment_module_id, :deployment_parameters, :approval_count, :allow_self_approval, approval_group_ids: [],
                                          # resource_ticketing_system_permissions_attributes: [:id, :enabled, :ticketing_system_permission_id],
                                          # environments_attributes: [:id, :name, :deployment_order, deployment_parameters: {}])
      safe_param.merge(
          deployment_parameters: YAML.load(params[:application][:deployment_parameters]) || {},
          environments_attributes: environments_deployment_parameters_to_hash(params[:application][:environments_attributes])
      )
    end

    def environments_deployment_parameters_to_hash(environments_attributes)
      if environments_attributes
        environments_attributes.keys.each do |k|
          if environments_attributes[k][:deployment_parameters].is_a?(String)
            environments_attributes[k][:deployment_parameters] = YAML.load(environments_attributes[k][:deployment_parameters]) || {}
          end
        end
      end

      environments_attributes || []
    end
end
