class EnvironmentsController < ApplicationController
  load_and_authorize_resource

  before_action :set_environment, only: [:show]
  before_action :set_application

  # GET /environments
  def index
    @deployments = @application.deployments
  end

  # GET /environments/1
  # GET /environments/1.json
  def show
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_environment
    @environment = Environment.find(params[:id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_application
    @application = Application.find(params[:application_id])
  end
end
