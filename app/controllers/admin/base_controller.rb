class Admin::BaseController  < ApplicationController
  def current_ability
    @current_ability ||= Ability.new(current_user)
  end
end