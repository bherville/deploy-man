class Admin::RolesController < Admin::BaseController
  load_and_authorize_resource

  def index
    @roles = Role.all.sort_by { |r| r.name }
  end

  def show
    @role = Role.find(params[:id])
  end
end
