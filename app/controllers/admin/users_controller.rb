class Admin::UsersController < Admin::BaseController
  load_and_authorize_resource

  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.save
      flash[:notice] = "t('admin.users.created_user') - #{@user.email}"
      redirect_to admin_users_path
    else
      render :action => 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
    @roles = @user.roles
  end

  def update
    @user = User.find(params[:id])
    user_account_params = user_params
    user_account_params.delete(:password) if user_account_params[:password].blank?
    user_account_params.delete(:password_confirmation) if user_account_params[:password].blank? and user_account_params[:password_confirmation].blank?

    respond_to do |format|
      if @user.update_attributes(user_account_params)
        format.html { redirect_to session[:user_return_to] ? session[:user_return_to] : admin_users_path, notice: "#{t('admin.users.updated_user')} - #{@user.email}" }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    if current_user == @user
      flash[:alert] = "#{t('admin.users.cannot_delete')} - #{@user.email}"
    elsif @user.destroy
      flash[:notice] = "#{t('admin.users.deleted_user')} - #{@user.email}"
    end

    respond_to do |format|
      format.html { redirect_to admin_users_path }
      format.json { head :no_content }
    end
  end

  private
  def user_params
    params.require(:user).permit(:password, :password_confirmation, :time_zone, :fname, :lname, :role_ids => [])
  end
end