class Admin::ApprovalGroupsController < Admin::BaseController
  load_and_authorize_resource

  def index
    @approval_groups = ApprovalGroup.all
  end

  def show
    @approval_group = ApprovalGroup.find(params[:id])
  end

  def new
    @approval_group = ApprovalGroup.new
    @approvers = User.all.select { |u| u.has_role?(:approver) } || []
  end

  def create
    @approval_group = ApprovalGroup.new(approval_group_params)
    @approvers = User.all.select { |u| u.has_role?(:approver) } || []

    if @approval_group.save
      flash[:notice] = "#{t('approval_groups.created')} - #{@approval_group.name}"
      redirect_to admin_approval_groups_path
    else
      render :action => 'new'
    end
  end

  def edit
    @approval_group = ApprovalGroup.find(params[:id])
    @approvers = User.all.select { |u| u.has_role?(:approver) } || []
  end

  def update
    @approval_group = ApprovalGroup.find(params[:id])
    @approvers = User.all.select { |u| u.has_role?(:approver) } || []

    respond_to do |format|
      if @approval_group.update_attributes(approval_group_params)
        format.html { redirect_to session[:user_return_to] ? session[:user_return_to] : admin_approval_groups_path, notice: "#{t('approval_groups.updated')} - #{@approval_group.name}" }
      else
        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    @approval_group = ApprovalGroup.find(params[:id]).destroy

    flash[:notice] = "#{t('approval_groups.deleted')} - #{@approval_group.name}"

    respond_to do |format|
      format.html { redirect_to admin_approval_groups_path }
      format.json { head :no_content }
    end
  end

  private
  def approval_group_params
    params.require(:approval_group).permit(:name, :description, user_ids: [])
  end
end
