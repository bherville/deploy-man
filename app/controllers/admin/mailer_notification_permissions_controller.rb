class Admin::MailerNotificationPermissionsController < ApplicationController
  load_and_authorize_resource

  def index
    @mailer_notification_permissions = MailerNotificationPermission.all.sort_by { |p| p.name }
  end
end
