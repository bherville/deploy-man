class Admin::AdminController < Admin::BaseController
  authorize_resource :class => false

  def index
    @admin_pages = Admin::ADMIN_PAGES

    respond_to do |format|
      format.html
    end
  end
end