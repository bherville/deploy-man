class Admin::DeploymentModulesController < Admin::BaseController
  load_and_authorize_resource

  def index
    @deployment_modules = DeploymentModule.all
    @available_deployment_modules = DeploymentModule.available_modules
  end

  def show
    @deployment_module = DeploymentModule.find(params[:id])
  end

  def edit
    @deployment_module  = DeploymentModule.find(params[:id])

    @parameters_hash = collect_deployment_parameters
  end

  def update
    @deployment_module = DeploymentModule.find(params[:id])

    respond_to do |format|
      if @deployment_module.update_attributes(deployment_module_params)
        format.html { redirect_to session[:user_return_to] ? session[:user_return_to] : admin_deployment_modules_path, notice: "#{t('deployment_modules.updated')} - #{@deployment_module.name}" }
      else
        @parameters_hash = collect_deployment_parameters

        format.html { render action: "edit" }
      end
    end
  end

  def destroy
    @deployment_module = DeploymentModule.find(params[:id]).destroy

    flash[:notice] = "#{t('deployment_modules.deleted')} - #{@deployment_module.name}"

    respond_to do |format|
      format.html { redirect_to admin_deployment_modules_path }
      format.json { head :no_content }
    end
  end

  def upload
    @deployment_module = DeploymentModule.new
  end

  def perform_upload
    respond_to do |format|
      @deployment_module = DeploymentModule.upload_and_install(params[:deployment_module][:module_archive_file].tempfile.path)

      if @deployment_module.errors.none? && @deployment_module.save
        format.html { redirect_to edit_admin_deployment_module_path(@deployment_module), notice: "#{t('deployment_modules.installed')} - #{@deployment_module.name}" }
      else
        format.html { render action: 'upload' }
      end
    end
  end

  def upgrade
    @deployment_module = DeploymentModule.find(params[:id])
  end

  def perform_upgrade
    @deployment_module = DeploymentModule.find(params[:id])

    respond_to do |format|
      if @deployment_module.upload_and_upgrade(params[:deployment_module][:module_archive_file].tempfile.path) && @deployment_module.errors.none? && @deployment_module.save
        format.html { redirect_to edit_admin_deployment_module_path(@deployment_module), notice: "#{t('deployment_modules.upgraded')} - #{@deployment_module.name}" }
      else
        format.html { render action: 'upload' }
      end
    end
  end

  private
  def deployment_module_params
    params.require(:deployment_module).permit(:name, :description, :module_path, :module_version).merge( {
                                                                                                             settings:                          YAML.load(params[:deployment_module][:settings]) || {},
                                                                                                             default_parameters_applications:   YAML.load(params[:deployment_module][:default_parameters_applications]) || {},
                                                                                                             default_parameters_deployments:    YAML.load(params[:deployment_module][:default_parameters_deployments]) || {},
                                                                                                             default_parameters_environments:   YAML.load(params[:deployment_module][:default_parameters_environments]) || {},
                                                                                                         })
  end

  def collect_deployment_parameters
    {
        settings:                         params[:settings]                         || deployment_parameters_to_string(@deployment_module.settings.to_yaml),
        default_parameters_applications:  params[:default_parameters_applications]  || deployment_parameters_to_string(@deployment_module.default_parameters_applications),
        default_parameters_deployments:   params[:default_parameters_deployments]   || deployment_parameters_to_string(@deployment_module.default_parameters_deployments),
        default_parameters_environments:  params[:default_parameters_environments]  || deployment_parameters_to_string(@deployment_module.default_parameters_environments),
    }
  end
end
