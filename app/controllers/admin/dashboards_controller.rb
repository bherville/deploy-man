class Admin::DashboardsController < ApplicationController
  load_and_authorize_resource find_by: :slug

  # GET /dashboards
  def index
    @dashboards = Dashboard.all.sort_by { |d| d.name }
  end
end
