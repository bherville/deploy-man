class Admin::AuditsController < ApplicationController
  authorize_resource :class => false
  before_action :set_audit, only: [:show]

  def index
  end

  def show
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_audit
    @audit = Audited.audit_class.find(params[:id])
  end
end
