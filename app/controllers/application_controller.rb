class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session

  include PublicActivity::StoreController
  include ActionView::Helpers::TextHelper

  before_filter :set_timezone

  rescue_from CanCan::AccessDenied do |exception|
    if user_signed_in?
      session[:user_return_to] = nil

      if params[:redirect_to]
        redirect_path = params[:redirect_to]
      else
        redirect_path = root_path
      end

      respond_to do |format|
        format.html { redirect_to redirect_path, :flash => { alert: t('user.not_authorized') } }
        format.json { render json: { :message => t('user.not_authorized'), :url => request.url } }
        format.js { render js: "displayAlert('#{t('user.not_authorized_action') % [params[:action], params[:controller].titleize]}!');", :status => :forbidden }
      end

    else
      session[:user_return_to] = request.url

      respond_to do |format|
        format.html { redirect_to '/users/sign_in', :flash => { alert: t('user.login_first') } }
        format.json { render json: { :message => t('user.login_first'), :url => request.url } }
      end
    end
  end

  def set_timezone
    Time.zone = current_user.time_zone if current_user
  end

  def deployment_parameters_to_string(deployment_parameters)
    if deployment_parameters.is_a?(Hash)
      params = ''

      deployment_parameters.each do |k,v|
        params += "#{k}: #{v}\n"
      end

      params
    else
      deployment_parameters
    end
  end

  private
  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end

  def after_sign_in_path_for(resource)
    session[:omniauth_redirect_to] || stored_location_for(resource) || root_path
  end
end