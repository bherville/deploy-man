class DeploymentsController < ApplicationController
  load_and_authorize_resource

  before_action :set_application
  before_action :set_deployment, only: [:show, :edit, :update, :destroy]
  before_action :set_cc_users, only: [:edit, :update]
  before_action :set_deployment_templates, only: [:new, :create, :edit, :update]

  # GET /deployments
  def index
    @deployments = @application.deployments
  end

  # GET /deployments/1
  # GET /deployments/1.json
  def show
  end

  # GET /applications/:application_id/deployments/new
  def new
    @deployment = @application.deployments.new

    @deployment.add_ticketable_permissions

    @deployment_parameters = deployment_parameters_to_string(@deployment.application.deployment_module.default_parameters_deployments.merge(@deployment.deployment_parameters))
  end

  # GET /applications/:application_id/deployments/1/edit
  def edit
    @deployment_parameters = params[:deployment_parameters] || deployment_parameters_to_string(@deployment.deployment_parameters)
  end

  # POST /applications/:application_id/deployments
  def create
    @deployment = @application.deployments.new(deployment_params)
    @deployment.user = current_user

    respond_to do |format|
      if @deployment.save
        format.html { redirect_to application_deployment_path(@deployment.application, @deployment), notice: 'Deployment was successfully created.' }
      else
        @deployment_parameters = params[:deployment_parameters] || deployment_parameters_to_string(@deployment.deployment_parameters)
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /applications/:application_id/deployments/1
  def update
    respond_to do |format|
      if @deployment.update(deployment_params)
        format.html { redirect_to application_deployment_path(@deployment.application, @deployment), notice: 'Deployment was successfully updated.' }
      else
        @deployment_parameters = params[:deployment_parameters] || deployment_parameters_to_string(@deployment.deployment_parameters)
        format.html { render :edit }
      end
    end
  end

  # DELETE /applications/:application_id/deployments/1
  def destroy
    @deployment.destroy
    respond_to do |format|
      format.html { redirect_to application_deployments_path(@application), notice: 'Deployment was successfully destroyed.' }
    end
  end

  # PUT/PATCH /applications/:application_id/deployments/:id/deploy.json
  def deploy
    @force_deploy = params[:force]
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deployment
      @deployments = @application.deployments.find(params[:id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_cc_users
      @cc_users = @deployment.cc_users
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_application
      @application = Application.find(params[:application_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_deployment_templates
      @deployment_templates = (DeploymentTemplate.select { |dt| dt.public } + current_user.deployment_templates).uniq
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deployment_params
      params.require(:deployment).permit(:comment, :ticket, :deployment_template_id, :post_deployment_ticketing_system_user, :akamai_cache_purge, cc_user_ids: [], environment_ids: [], resource_ticketing_system_permissions_attributes: [:id, :enabled, :ticketing_system_permission_id]).merge(deployment_parameters: (YAML.load(params[:deployment][:deployment_parameters]) || {}))
    end
end
