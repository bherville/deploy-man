class ApprovalGroupsController < ApplicationController
  load_and_authorize_resource

  def index
    @approval_groups = ApprovalGroup.all
  end

  def show
    @approval_group = ApprovalGroup.find(params[:id])
  end
end
