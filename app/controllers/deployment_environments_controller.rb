require 'reloader/sse'

class DeploymentEnvironmentsController < ApplicationController
  include ActionController::Live

  load_and_authorize_resource
  before_action :set_deployment_environment, only: [:output, :schedule, :cancel_schedule, :update_schedule]


  # GET /applications/:application_id/deployments/:deployment_id/deployment_environments
  def index
    case request.format.to_sym
      when :js
        if params[:ids]
          stream_index(params[:ids].split(','))
        else
          # Throw exception
        end
      else
        respond_to do |format|
          format.html
        end
    end
  end

  def output
    case request.format.to_sym
      when :js
        steam_output
      else
        respond_to do |format|
          format.html
        end
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_deployment_environment
    @deployment_environment = DeploymentEnvironment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def deployment_environment_params
    params.require(:deployment_environment).permit(:deploy_at)
  end

  def steam_output
    # SSE expects the `text/event-stream` content type
    response.headers['Content-Type'] = 'text/event-stream'

    sse = Reloader::SSE.new(response.stream)

    begin
      previous_size = 0
      current_run = -1

      output = nil
      previous_deployment_status = nil
      current_deployment_status = nil
      previous_deployment_comment = nil
      current_deployment_comment = nil

      loop do
        DeploymentEnvironment.uncached do
          de = DeploymentEnvironment.find(params[:id])

          output = de.output_log

          current_deployment_status = de.current_state
          current_deployment_comment = de.deployer_comment
        end


        if previous_size < output.size
          to_show = output[previous_size, output.size]
          sse.write(create_stream_message(:message,  { data_type: :deployment_environment_output_log, data: { deployment_environment_id: @deployment_environment.id, output_log: to_show }}))

          previous_size = output.size
        end

        unless previous_deployment_status == current_deployment_status
          sse.write(create_stream_message(:message, { data_type: :deployment_environment_deployment_status, data: { deployment_environment_id: @deployment_environment.id, deployment_status: current_deployment_status }}))

          previous_deployment_status = current_deployment_status
        end

        unless previous_deployment_comment == current_deployment_comment
          sse.write(create_stream_message(:message, { data_type: :deployment_environment_deployer_comment, data: { deployment_environment_id: @deployment_environment.id, deployer_comment: current_deployment_comment }}))

          previous_deployment_comment = current_deployment_comment
        end

        sleep 1

        if current_run == 4 || current_run == -1
          sse.write(create_stream_message(:heart_beat, { data_type: :timestamp, data: Time.now.to_i }))
          current_run = 0
        else
          current_run += 1
        end

        if @deployment_environment.deployed? || @deployment_environment.failed?
          sse.write(create_stream_message(:complete, { data_type: :timestamp, data: Time.now.to_i }))

          sleep 1
          break
        end
      end
    rescue ActionController::Live::ClientDisconnected => e
      # When the client disconnects, we'll get an IOError on write
    ensure
      sse.close
    end
  end

  def stream_index(deployment_environment_ids)
    # SSE expects the `text/event-stream` content type
    response.headers['Content-Type'] = 'text/event-stream'

    sse = Reloader::SSE.new(response.stream)
    begin
      current_run = -1

      de_state_hash = {}
      deployment_environment_ids.each { |de_id| de_state_hash[de_id] = nil }

      loop do
        deployment_environment_ids.each do |deployment_environment_id|
          deployment_environment_id = deployment_environment_id.strip

          current_workflow_state = nil
          de = nil

          DeploymentEnvironment.uncached do
            de = DeploymentEnvironment.find(deployment_environment_id)

            current_workflow_state = de.workflow_state
          end

          unless de_state_hash[deployment_environment_id] == current_workflow_state
            sse.write(create_stream_message(:message, { data_type: :deployment_environment, data: { deployment_environment: de.to_json(methods: [:deploy_at_formatted]) }}))

            de_state_hash[deployment_environment_id] = current_workflow_state
          end
        end
        sleep 1

        if current_run == 4 || current_run == -1
          sse.write(create_stream_message(:heart_beat, { data_type: :timestamp, data: Time.now.to_i }))
          current_run = 0
        else
          current_run += 1
        end
      end
    rescue ActionController::Live::ClientDisconnected => e
      # When the client disconnects, we'll get an IOError on write
    ensure
      sse.close
    end
  end

  def create_stream_message(message_type, data_hash)
    { message_type: message_type, message_data: data_hash }
  end
end
