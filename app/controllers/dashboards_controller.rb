class DashboardsController < ApplicationController
  load_and_authorize_resource find_by: :slug
  before_action :allowed_dashboard?, only: [:show, :home]
  before_action :set_dashboard, only: :show

  # GET /dashboards
  def index
    @dashboards = Dashboard.where('user_id=? OR public=?', current_user.id, true).sort_by { |d| d.name }
  end

  # GET /dashboards/1
  def show
    respond_to_dashboard @dashboard
  end

  # GET /dashboards/home
  def home
    if current_user.default_dashboard
      @dashboard = current_user.default_dashboard
    else
      @dashboard = Dashboard.system_default_dashboard
    end

    respond_to_dashboard @dashboard
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_dashboard
    @dashboard = Dashboard.friendly.find(params[:id])
  end

  def respond_to_dashboard(dashboard)
    respond_to do |format|
      format.html {
        if @dashboard.view_path
          render @dashboard.view_path
        else
          render inline: @dashboard.view_html
        end
      }
    end
  end

  def allowed_dashboard?
    case params[:action]
      when :home, 'home'
        dashboard_id = current_user.default_dashboard ? current_user.default_dashboard.name : Dashboard.system_default_dashboard.name
      else
        dashboard_id = params[:id]
    end

    unless Dashboard.where('lower(name)=? AND (user_id=? OR public=?)', dashboard_id.downcase, current_user.id, true).count > 0
      respond_to do |format|
        format.html { redirect_to Dashboard.system_default_dashboard, alert: "You do not have access to the dashboard #{dashboard_id}" }
      end
    end
  end
end
