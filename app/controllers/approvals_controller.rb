class ApprovalsController < ApplicationController
  load_and_authorize_resource
  before_action :set_approval, :except => [:index, :waiting]
  before_action :set_deployment, :except => [:waiting]
  before_action :set_application, :except => [:waiting]

  # GET /approvals
  def index
    @approvals = @deployment.approvals
  end

  # GET /approvals/1
  def show
  end

  def waiting
    @approvals = Approval.approvals_for_user(current_user)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_approval
      @approval = Approval.find(params[:id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_application
      @application = Application.find(params[:application_id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_deployment
      @deployment = Deployment.find(params[:deployment_id])
    end
end
