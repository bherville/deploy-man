class DeploymentTemplatesController < ApplicationController
  load_and_authorize_resource
  before_action :set_deployment_template, only: [:show, :edit, :update, :destroy]
  before_action :set_cc_users, only: [:edit, :update]
  before_action :owner?, only: [:edit, :update, :destroy]

  # GET /deployment_templates
  def index
    if params[:personal]
      @deployment_templates = current_user.deployment_templates
    else
      @deployment_templates = (DeploymentTemplate.select { |dt| dt.public } + current_user.deployment_templates).uniq
    end
  end

  # GET /deployment_templates/1
  def show
  end

  # GET /deployment_templates/new
  def new
    clone_template_params = {}

    if params[:clone_template_id]
      dt = DeploymentTemplate.find(params[:clone_template_id])
      clone_template_params = dt.clone_params if dt
    end

    @deployment_template = current_user.deployment_templates.new(clone_template_params)
  end

  # GET /deployment_templates/1/edit
  def edit
  end

  # POST /deployment_templates
  def create
    @deployment_template = current_user.deployment_templates.new(deployment_template_params)

    respond_to do |format|
      if @deployment_template.save
        format.html { redirect_to @deployment_template, notice: 'Deployment template was successfully created.' }
      else
        format.html { render :new }
      end
    end
  end

  # PATCH/PUT /deployment_templates/1
  def update
    respond_to do |format|
      if @deployment_template.update(deployment_template_params)
        format.html { redirect_to @deployment_template, notice: 'Deployment template was successfully updated.' }
      else
        format.html { render :edit }
      end
    end
  end

  # DELETE /deployment_templates/1
  def destroy
    @deployment_template.destroy
    respond_to do |format|
      format.html { redirect_to deployment_templates_url, notice: 'Deployment template was successfully destroyed.' }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_deployment_template
      @deployment_template = DeploymentTemplate.find(params[:id])
    end

    # Use callbacks to share common setup or constraints between actions.
    def set_cc_users
      @cc_users = @deployment_template.cc_users || []
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def deployment_template_params
      params.require(:deployment_template).permit(:name, :comment, :deployment_parameters, :public, :ticket, :application_id, :post_deployment_ticketing_system_user, cc_user_ids: [])
    end

    def owner?
      @deployment_template.user == current_user || current_user.has_role?(:admin)
    end
end
