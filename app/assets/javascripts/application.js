// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require swagger-ui
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require look_and_feel
//= require bootstrap-dialog.min
//= require deployments.coffee.erb
//= require highcharts
//= require chartkick
//= require jquery.cookie
//= require applications.coffee.erb
//= require admin/audits.js.erb
//= require select2.min
//= require watch_unwatch.js.erb

$(document).ready(function() {
    if ($.cookie("deployman_new_session_events") != '1') {
        new_session_events();
    }
});

function new_session_events() {
    $.ajax({
        url: "/api/approvals/waiting",
        dataType: "json",
    }).success(function(data) {
        console.log(data.length);
        if (data.length > 0) {
            var approval_link = "<a href='" + "/approvals/waiting" + "'>" + data.length + "</a>";
            displayNotice("You have " + approval_link + " approvals waiting.");
        }
    });


    $.cookie('deployman_new_session_events', '1');
}

String.prototype.titleize = function() {
    var words = this.split('_');
    var array = [];
    for (var i=0; i<words.length; ++i) {
        array.push(words[i].charAt(0).toUpperCase() + words[i].toLowerCase().slice(1))
    }
    return array.join(' ')
};