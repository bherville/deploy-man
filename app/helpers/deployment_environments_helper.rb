module DeploymentEnvironmentsHelper
  def link_to_trackable_deployment_environment(object, object_type)
    if object
      "the #{object.environment.name} environment #{link_to object.id, application_deployment_path(object.deployment.application, object.deployment)} deployment for #{link_to object.deployment.application.name, object.deployment.application}".html_safe
    else
      t('applications.for_an_application')
    end
  end

  def approval_status(deployment_environment)
    if deployment_environment.awaiting_approval?
      t('deployment_environment.awaiting_approval')
    elsif deployment_environment.rejected?
      t('deployment_environment.rejected')
    else
      t('deployment_environment.approved')
    end
  end
end
