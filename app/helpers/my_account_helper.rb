module MyAccountHelper
  def avatar_url(user, size = 80)
    user.gravatar_url(:size => size)
  end

  def show_avatar
    APP_CONFIG["user_profiles"].present? && APP_CONFIG["user_profiles"]["show_avatars"]
  end

  def users_valid_dashboards
    Dashboard.where('user_id=? OR public=?', current_user.id, true).sort_by { |d| d.name }
  end
end
