module ApplicationsHelper
  def environment_deployment_params(environment_index, application, environment)
    if params[:application] && params[:application][:environments_attributes] && params[:application][:environments_attributes][environment_index.to_s]
      if params[:application][:environments_attributes][environment_index.to_s]['deployment_parameters'].is_a?(String)
        params[:application][:environments_attributes][environment_index.to_s]['deployment_parameters']
      else
        deployment_parameters_to_string(params[:application][:environments_attributes][environment_index.to_s]['deployment_parameters'])
      end
    else
      application.deployment_module ? application.deployment_module.default_parameters_environments.merge(environment.deployment_parameters).to_yaml : environment.deployment_parameters.to_yaml
    end
  end

  def akamai_cp_codes_to_string(akamai_cp_codes_array)
    akamai_cp_codes_array ? akamai_cp_codes_array.join(', ') : ''
  end
end
