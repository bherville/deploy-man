module ApplicationHelper
  def boolean_to_string(bool)
    case bool
      when 0, false, 'false', 'FALSE', 'False'
        t('general.boolean_no')
      when 1, true, 'true', 'TRUE', 'True'
        t('general.boolean_yes')
    end
  end

  def deployment_parameters_to_string(deployment_parameters)
    if deployment_parameters.is_a?(Hash)
      params = ''

      deployment_parameters.each do |k,v|
        params += "#{k}: #{v}\n"
      end

      params
    else
      deployment_parameters
    end
  end

  def cc_users_js(cc_users)
    "<script>var previous_cc_users_array = [#{raw (cc_users.map{|u| { id: u.id, full_name: u.full_name, email: u.email, gravatar_url: u.gravatar_url }}).to_json}]</script>".html_safe
  end
end
