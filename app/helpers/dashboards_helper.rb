module DashboardsHelper
  def timeago(time, options = {})
    options[:class] ||= 'timeago'
    content_tag(:abbr, "#{time_ago_in_words(time.to_s)} ago", options.merge(:title => time.getutc.iso8601)) if time
  end

  def whose?(user, object)
    case object
      when Application
        owner = object.user
      else
        owner = nil
    end
    if user and owner
      if user.id == owner.id
        t('activity.whose.own')
      else
        "#{owner.name}'s"
      end
    else
      ''
    end
  end

  # Check if object still exists in the database and display a link to it,
  # otherwise display a proper message about it.
  # This is used in activities that can refer to
  # objects which no longer exist, like removed posts.
  def link_to_trackable(object, object_type, options={})
    if object
      "#{object_type.downcase} #{link_to object.to_s, options[:link] || object}".html_safe
    else
      "#{object_type.downcase} which does not exist anymore"
    end
  end
end
