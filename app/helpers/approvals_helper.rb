module ApprovalsHelper
  def show_approve_reject_links(approval)
    approval.can_approve_reject?(current_user) && !approval.already_approved_rejected?
  end

  # Check if object still exists in the database and display a link to it,
  # otherwise display a proper message about it.
  # This is used in activities that can refer to
  # objects which no longer exist, like removed posts.
  def link_to_trackable_approval(object, object_type)
    if object && object.deployment && object.deployment.application
      "#{link_to object.deployment.id, application_deployment_path(object.deployment.application, object.deployment)} for #{link_to object.deployment.application.name, object.deployment.application}".html_safe
    else
      "for an application which does not exist anymore"
    end
  end
end
