module AkamaiPurgeMailerHelper
  def akamai_purge_results_hash_to_list(purge_results)
    list = ''

    purge_results.each do |key, value|
      list += "#{key.to_s.titleize}: #{value}\n"
    end

    list
  end

  def akamai_purge_results_hash_to_html(purge_results)
    list = ''

    purge_results.each do |key, value|
      list += "#{key.to_s.titleize}: #{value}<br />"
    end

    list
  end
end
