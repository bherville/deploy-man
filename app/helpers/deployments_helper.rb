module DeploymentsHelper
  # Check if object still exists in the database and display a link to it,
  # otherwise display a proper message about it.
  # This is used in activities that can refer to
  # objects which no longer exist, like removed posts.
  def link_to_trackable_deployment(object, object_type)
    if object
      "#{link_to object.id, application_deployment_path(object.application, object)} for #{link_to object.application.name, object.application}".html_safe
    else
      "#{link_to object.id, application_deployment_path(object.application, object)} for #{link_to object.application.name, object.application} which does not exist anymore"
    end
  end
end
