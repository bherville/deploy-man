require 'test_helper'

class DeploymentTemplatesControllerTest < ActionController::TestCase
  setup do
    @deployment_template = deployment_templates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:deployment_templates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create deployment_template" do
    assert_difference('DeploymentTemplate.count') do
      post :create, deployment_template: { comment: @deployment_template.comment, deployment_parameters: @deployment_template.deployment_parameters, name: @deployment_template.name, public: @deployment_template.public, user_id: @deployment_template.user_id }
    end

    assert_redirected_to deployment_template_path(assigns(:deployment_template))
  end

  test "should show deployment_template" do
    get :show, id: @deployment_template
    assert_response :success
  end

  test "should get edit" do
    get :schedule, id: @deployment_template
    assert_response :success
  end

  test "should update deployment_template" do
    patch :update, id: @deployment_template, deployment_template: { comment: @deployment_template.comment, deployment_parameters: @deployment_template.deployment_parameters, name: @deployment_template.name, public: @deployment_template.public, user_id: @deployment_template.user_id }
    assert_redirected_to deployment_template_path(assigns(:deployment_template))
  end

  test "should destroy deployment_template" do
    assert_difference('DeploymentTemplate.count', -1) do
      delete :destroy, id: @deployment_template
    end

    assert_redirected_to deployment_templates_path
  end
end
