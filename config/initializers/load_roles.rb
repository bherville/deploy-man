def create_default_resource_roles
    # Admin Resource Roles
    Role.find_by_name(:admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :admin)
    Role.find_by_name(:admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :mailer_notification_permission, resource_type: :class)
    Role.find_by_name(:admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :approval_group, resource_type: :class)
    Role.find_by_name(:admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS.push([:upload, :perform_upload, :upgrade, :perform_upgrade])).find_or_create_by(resource_name: :deployment_module, resource_type: :class)
    Role.find_by_name(:admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :deployment_template, resource_type: :class)
    Role.find_by_name(:admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :role, resource_type: :class)
    Role.find_by_name(:admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :user, resource_type: :class)

    # Auditor Resource Roles
    Role.find_by_name(:auditor).resource_roles.create_with(permissions: [:read]).find_or_create_by(resource_name: :audit)

    # Normal User Resource Roles
    Role.find_by_name(:normal_user).resource_roles.create_with(permissions: [:read, :update]).find_or_create_by(resource_name: :my_account)
    Role.find_by_name(:normal_user).resource_roles.create_with(permissions: [:read, :approvals_per_deployment, :deployments_per_application, :deployments_created_last_x_days]).find_or_create_by(resource_name: :chart)
    Role.find_by_name(:normal_user).resource_roles.create_with(permissions: [:read, :home]).find_or_create_by(resource_name: :dashboard, resource_type: :class)
    Role.find_by_name(:normal_user).resource_roles.create_with(permissions: [:read]).find_or_create_by(resource_name: :approval, resource_type: :class)
    Role.find_by_name(:normal_user).resource_roles.create_with(permissions: [:read, :watch, :unwatch]).find_or_create_by(resource_name: :application, resource_type: :class)
    Role.find_by_name(:normal_user).resource_roles.create_with(permissions: [:read, :watch, :unwatch]).find_or_create_by(resource_name: :deployment, resource_type: :class)
    Role.find_by_name(:normal_user).resource_roles.create_with(permissions: [:read]).find_or_create_by(resource_name: :environment, resource_type: :class)
    Role.find_by_name(:normal_user).resource_roles.create_with(permissions: [:read, :output]).find_or_create_by(resource_name: :deployment_environment, resource_type: :class)

    # Application Admins Role
    Role.find_by_name(:application_admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :application, resource_type: :class)
    Role.find_by_name(:application_admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :environment, resource_type: :class)
    Role.find_by_name(:application_admin).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :deployment_environment, resource_type: :class)
    Role.find_by_name(:application_admin).resource_roles.create_with(permissions: [:read]).find_or_create_by(resource_name: :user, resource_type: :class)

    # Deployer Roles
    Role.find_by_name(:deployer).resource_roles.create_with(permissions: [:create, :read, :update]).find_or_create_by(resource_name: :deployment, resource_type: :class)
    Role.find_by_name(:deployer).resource_roles.create_with(permissions: [:read]).find_or_create_by(resource_name: :environment, resource_type: :class)
    Role.find_by_name(:deployer).resource_roles.create_with(permissions: [:create, :read, :update, :deploy, :schedule, :update_schedule, :cancel_schedule]).find_or_create_by(resource_name: :deployment_environment, resource_type: :class)
    Role.find_by_name(:deployer).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :deployment_template, resource_type: :class)
    Role.find_by_name(:application_admin).resource_roles.create_with(permissions: [:read]).find_or_create_by(resource_name: :user, resource_type: :class)


    # API Roles
    # Approver Resource Roles
    Role.find_by_name(:approver).resource_roles.create_with(permissions: [:read, :approve, :reject, :waiting]).find_or_create_by(resource_name: :approval, resource_type: :class)
    Role.find_by_name(:approver).resource_roles.create_with(permissions: [:read]).find_or_create_by(resource_name: :deployment, resource_type: :class)

    # Approval Admins
    Role.find_by_name(:approval_admin).resource_roles.create_with(permissions: [:read, :approve, :reject, :waiting]).find_or_create_by(resource_name: :approval, resource_type: :class)

    Role.find_by_name(:api_readonly).resource_roles.create_with(permissions: [:read]).find_or_create_by(resource_name: :api)
    Role.find_by_name(:api_read_write).resource_roles.create_with(permissions: ResourceRole::ALL_CRUD_PERMISSIONS).find_or_create_by(resource_name: :api)
end

if ActiveRecord::Base.connection.table_exists? 'roles'
    BUILT_IN_ROLES = [
        Role.find_or_create_by(
            name:           :admin,
            description:    'Can administrate the system',
            built_in:       true,

        ),
        Role.find_or_create_by(
            name:           :api_readonly,
            description:    "Can access the API's read only endpoints",
            built_in:       true,
        ),
        Role.find_or_create_by(
            name:           :api_read_write,
            description:    "Can access the API's read and write endpoints",
            built_in:       true,
        ),
        Role.find_or_create_by(
            name:           :approver,
            description:    'Can approve deployments',
            built_in:       true,
        ),
        Role.find_or_create_by(
            name:           :auditor,
            description:    'Can manage audits',
            built_in:       true,
        ),
        Role.find_or_create_by(
            name:           :approval_admin,
            description:    'Can administrate approvals deployments (can approve or reject any approvals)',
            built_in:       true,
        ),
        Role.find_or_create_by(
            name:           :application_admin,
            description:    'Can manage Applications',
            built_in:       true,
        ),
        Role.find_or_create_by(
            name:           :deployer,
            description:    'Can deploy Applications',
            built_in:       true,
        ),
        Role.find_or_create_by(
            name:           :normal_user,
            description:    'A non-privileged user of the system',
            built_in:       true,
        )
    ]

    DEFAULT_ROLES = [
        Role.find_by_name(:normal_user)
    ]

    create_default_resource_roles
end