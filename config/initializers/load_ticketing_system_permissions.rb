def ticketing_system_enabled?(system_name)
  AppConfig.get(%W(#{system_name.to_s} enable), APP_CONFIG) ? true : false
end

if ticketing_system_enabled?(:jira) && ActiveRecord::Base.connection.table_exists?('ticketing_system_permissions')
  BUILT_IN_TICKETING_SYSTEM_PERMISSIONS = {
      application: [
        TicketingSystemPermission.find_or_create_by(
          name: :comment_on_deployment_submitted,
          description: 'Comment when a deployment is submitted',
          built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_awaiting_approval,
            description: 'Comment when a deployment is awaiting approval',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_awaiting_deployment,
            description: 'Comment when a deployment is awaiting deployment',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_rejected,
            description: 'Comment when a deployment is rejected',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_queued,
            description: 'Comment when a deployment is queued for deployment',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_deploying,
            description: 'Comment when a deployment is being deployed',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_deployed,
            description: 'Comment when a deployment is deployed',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_failed,
            description: 'Comment when a deployment failed to be deployed',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :post_log_on_deployment_deployed,
            description: 'Post a text file containing the output log on successful deployment.',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :post_log_on_deployment_failed,
            description: 'Post a text file containing the output log on failed deployment.',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_akamai_cache_clearing,
            description: 'Comment when an Akamai cache clear is initiated for a deployment',
            built_in: true
        ),
        TicketingSystemPermission.find_or_create_by(
            name: :comment_on_deployment_akamai_cache_cleared,
            description: 'Comment when an Akamai cache clear is completed for a deployment',
            built_in: true
        )
      ],
      deployment: [
          TicketingSystemPermission.find_or_create_by(
              name: :comment,
              description: 'Comment on the tickets',
              built_in: true
          ),
          TicketingSystemPermission.find_or_create_by(
              name: :post_output_log,
              description: 'Post the output log to the ticket',
              built_in: true
          ),
          TicketingSystemPermission.find_or_create_by(
              name: :reassign_ticket_on_deployed,
              description: 'Reassign the associated ticket on successful deployment',
              built_in: true
          ),
          TicketingSystemPermission.find_or_create_by(
              name: :reassign_ticket_on_failed,
              description: 'Reassign the associated ticket on failed deployment',
              built_in: true
          )
      ]
  }

  def add_ticketing_system_permissions(resource, resource_type)
    BUILT_IN_TICKETING_SYSTEM_PERMISSIONS[resource_type].each do |tsp|
      temp_tsp = TicketingSystemPermission.find_by_name(tsp.name)

      resource.ticketing_system_permissions << temp_tsp unless resource.ticketing_system_permissions.include?(temp_tsp)
    end
  end
end