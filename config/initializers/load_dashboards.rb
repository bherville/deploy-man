if ActiveRecord::Base.connection.table_exists? 'dashboards'
  BUILT_IN_DASHBOARDS = [
      Dashboard.find_or_create_by!(
                   name: 'System',
                   description: 'System Statistics Dashboard',
                   view_path: 'dashboards/dashboards/system',
                   public: true,
                   built_in: true,
                   system_default: true
      )
  ]

  SYSTEM_DEFAULT_DASHBOARD = Dashboard.find_by_name 'System'
end