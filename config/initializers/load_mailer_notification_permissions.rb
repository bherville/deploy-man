if ActiveRecord::Base.connection.table_exists? 'mailer_notification_permissions'
    BUILT_IN_MAILER_NOTIFICATION_PERMISSIONS= [
        MailerNotificationPermission.find_or_create_by(
            name:           :send_new_application,
            description:    'Send new application notifications',
            mailer_name:    :applications_mailer,
            method_name:    :new_notification,
            built_in:       true

        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_destroy_application,
            description:    'Send application destroyed notifications',
            mailer_name:    :applications_mailer,
            method_name:    :destroy_notification,
            built_in:       true

        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_new_deployment,
            description:    'Send new deployment notifications',
            mailer_name:    :deployment_mailer,
            method_name:    :new_notification,
            built_in:       true

        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_scheduled_deployment,
            description:    'Send deployment scheduled notifications',
            mailer_name:    :deployment_environments_mailer,
            method_name:    :scheduled_notification,
            built_in:       true

        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_deploy_deployment_environment,
            description:    'Send notifications when a deployment is being deployed to an environment',
            mailer_name:    :deployment_environments_mailer,
            method_name:    :deploy_notification,
            built_in:       true

        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_deployed_deployment_environment,
            description:    'Send notifications when a deployment has either been deployed or has failed to be deployed to an environment',
            mailer_name:    :deployment_environments_mailer,
            method_name:    :deployed_notification,
            built_in:       true

        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_awaiting_approval_notification,
            description:    'Send approval waiting notification',
            mailer_name:    :approval_mailer,
            method_name:    :awaiting_approval_notification,
            built_in:       true

        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_deployment_approved_notification,
            description:    'Send deployment approved notification',
            mailer_name:    :approval_mailer,
            method_name:    :approved_notification,
            built_in:       true

        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_deployment_rejected_notification,
            description:    'Send deployment rejected notification',
            mailer_name:    :approval_mailer,
            method_name:    :rejected_notification,
            built_in:       true
        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_akamai_purge_initiated_notification,
            description:    'Send Akamai cache purge initiated notification',
            mailer_name:    :akamai_purge_mailer,
            method_name:    :purge_initiated_notification,
            built_in:       true
        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_akamai_purge_in_progress_notification,
            description:    'Send Akamai cache purge in progress notification',
            mailer_name:    :akamai_purge_mailer,
            method_name:    :purge_in_progress_notification,
            built_in:       true
        ),
        MailerNotificationPermission.find_or_create_by(
            name:           :send_akamai_purge_completed_notification,
            description:    'Send Akamai cache purge completed notification',
            mailer_name:    :akamai_purge_mailer,
            method_name:    :purge_completed_notification,
            built_in:       true
        )
    ]
end