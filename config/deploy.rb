# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'deploy_man'				                              # The application name
set :repo_url, 'git@bitbucket.org:bherring/deploy-man.git'	        # The repository to pull from
set :branch, ENV['TAG'] || 'master'					                        # The branch in the repository to deploy from
set :deploy_to, '/var/www/deployman'					                      # The directory to deploy to
set :log_level, :debug							                                # The logging level for capistrano
set :keep_releases, 5							                                  # The number of release to keep on the destination servers
set :gem_set, 'ruby-2.1.5@deployman'          				              # The RVM gemset to use for this application


# Add the base directory for DeploymentModules
# This should probably be set to shared_path
set :DEPLOYMENT_MODULE_BASE_DIRECTORY, shared_path



######################
# DO NOT EDIT BELLOW #
######################
# Shared
set :linked_files, %w{config/app_config.yml config/sidekiq.yml config/database.yml config/secrets.yml config/environments/production.rb config/initializers/devise.rb}
set :linked_dirs, %w{log vendor/deployment_modules}

# RVM
set :rvm_type, :system
set :rvm_ruby_version, fetch(:gem_set)

# Rails
set :rails_env, 'production'
set :migration_role, :primary_app
set :assets_roles, [:web, :app]

# Bundler
set :bundle_roles, :app
set :bundle_jobs, 4
set :bundle_flags, '--quiet --deployment'

# Git
# set :scm, :git
# set :git_strategy, Capistrano::Git::SubmoduleStrategy

namespace :deploy do
  desc 'Restart application'
  task :restart do
    on roles(:app) do
      execute :touch, current_path.join('tmp/restart.txt')

      execute :sudo, '/sbin/service deployman_job restart'
    end
  end

  desc 'Create Directories'
  task :create_directories do
    on roles(:app) do
      execute "[ -d #{shared_path.join('config')} ] || mkdir -p #{shared_path.join('config')}"
      execute "[ -d #{shared_path.join('config','environments')} ] || mkdir -p #{shared_path.join('config', 'environments')}"
      execute "[ -d #{shared_path.join('config','initializers')} ] || mkdir -p #{shared_path.join('config', 'initializers')}"
      execute "[ -d #{shared_path.join('vendor','deployment_modules')} ] || mkdir -p #{shared_path.join('vendor', 'deployment_modules')}"
    end
  end

  desc 'Upload Configurations'
  task :upload_configurations do
    on roles(:app) do
      # Create the config directory if it doesn't exist
      execute "[ -d #{shared_path.join('config')} ] || mkdir -p #{shared_path.join('config')}"

      # Upload the configuration files
      set :stage_source, "config/deploy/config/#{fetch(:stage)}"
      set :all_source, 'config/deploy/config/all'

      upload! "#{fetch(:stage_source)}/app_config.yml", shared_path.join('config/app_config.yml')
      upload! "#{fetch(:stage_source)}/sidekiq.yml", shared_path.join('config/sidekiq.yml')  if File.exists?("#{fetch(:stage_source)}/sidekiq.yml")
      upload! "#{fetch(:stage_source)}/database.yml", shared_path.join('config/database.yml')
      upload! "#{fetch(:stage_source)}/secrets.yml", shared_path.join('config/secrets.yml')
      upload! "#{fetch(:stage_source)}/environments/production.rb", shared_path.join('config/environments/production.rb')
      upload! "#{fetch(:stage_source)}/initializers/devise.rb", shared_path.join('config/initializers/devise.rb')
    end
  end

  desc 'Setup RVM'
  task :setup_rvm do
    on roles(:app) do
      # Create the RVM gemset if it doesn't already exist
      execute "cd #{shared_path.join('config')} && rvm gemset use #{fetch(:gem_set)} ; [ $? -eq 0 ] || rvm --ruby-version use #{fetch(:gem_set)} --create"
      execute "gem list --local | grep bundler > /dev/null ; [ $? -eq 1 ] || /usr/local/rvm/bin/rvm #{fetch(:gem_set)} do gem install bundler"

      # Add authman_base_url
      execute "grep -q -F \"export AUTHMAN_BASE_URL='#{fetch(:AUTHMAN_BASE_URL)}'\" /usr/local/rvm/gems/#{fetch(:gem_set)}/environment || echo \"export AUTHMAN_BASE_URL='#{fetch(:AUTHMAN_BASE_URL)}'\" >> /usr/local/rvm/gems/#{fetch(:gem_set)}/environment"

      # Add the base directory for DeploymentModules
      # This should probably be set to shared_path
      if fetch(:DEPLOYMENT_MODULE_BASE_DIRECTORY)
        execute "grep -q -F \"export DEPLOYMENT_MODULE_BASE_DIRECTORY='#{fetch(:DEPLOYMENT_MODULE_BASE_DIRECTORY)}'\" /usr/local/rvm/gems/#{fetch(:gem_set)}/environment || echo \"export DEPLOYMENT_MODULE_BASE_DIRECTORY='#{fetch(:DEPLOYMENT_MODULE_BASE_DIRECTORY)}'\" >> /usr/local/rvm/gems/#{fetch(:gem_set)}/environment"
      end

      if fetch(:GRAVATAR_DOMAIN)
        execute "grep -q -F \"export GRAVATAR_DOMAIN='#{fetch(:GRAVATAR_DOMAIN)}'\" /usr/local/rvm/gems/#{fetch(:gem_set)}/environment || echo \"export GRAVATAR_DOMAIN='#{fetch(:GRAVATAR_DOMAIN)}'\" >> /usr/local/rvm/gems/#{fetch(:gem_set)}/environment"
      end
    end
  end

  desc 'Post Migration'
  task :post_migration do
    on roles(:app) do
      execute "[ -d #{release_path.join('tmp')} ] || mkdir -p #{release_path.join('tmp')}"
      execute "[ -d #{release_path.join('tmp','pids')} ] || mkdir -p #{release_path.join('tmp','pids')}"

      execute "[ -d #{release_path.join('public')} ] || mkdir -p #{release_path.join('public')}"

      execute "cd #{release_path} && /usr/local/rvm/bin/rvm #{fetch(:gem_set)} do bundle exec rake swagger:docs RAILS_ENV='#{fetch(:rails_env)}' DEPLOY_MAN_HOSTNAME='#{fetch(:DEPLOY_MAN_HOSTNAME)}' DEPLOY_MAN_API_HOST='#{fetch(:DEPLOY_MAN_API_HOST)}'"
      execute "cd #{release_path} && /usr/local/rvm/bin/rvm #{fetch(:gem_set)} do bundle exec rake resource_roles:refresh RAILS_ENV='#{fetch(:rails_env)}'"
    end
  end

  task :fix_absent_manifest_bug do
    on roles(:web) do
      execute "[ -d #{release_path.join('public', fetch(:assets_prefix))} ] || mkdir -p #{release_path.join('public', fetch(:assets_prefix))}"
      execute :touch, release_path.join('public', fetch(:assets_prefix), 'manifest-fix.temp')
    end
  end


  # Cap hooks
  before 'rvm:hook', 'deploy:setup_rvm'
  before :starting, :create_directories
  after  :create_directories, :upload_configurations
  after :updating, 'deploy:fix_absent_manifest_bug'
  after :updated, 'bundler:install'
  after 'deploy:migrate', :post_migration
  after :finished, :restart
end