Rails.application.routes.draw do
  authenticated :user do
    root :to => 'dashboards#home', :as => :authenticated_root
  end

  root :to => redirect('/users/sign_in')

  devise_for :users, :controllers => { :users => 'users', :registrations => 'registrations', :confirmations => 'confirmations', :password_expired => 'password_expired', :omniauth_callbacks => 'users/omniauth_callbacks' }
  devise_scope :user do
    put '/confirm' => 'confirmations#confirm'
  end

  resource :my_account, :controller => :my_account, :only => [:show]  do
    resource :password_expired, :only => [:show, :update], :controller => :password_expired
  end

  resources :applications do
    resources :environments
    resources :deployments do
      resources :deployment_environments, only: [:index, :show, :output] do
        member do
          get :output
        end
        resources :approvals, :only => [:index, :show]
      end
    end
  end

  resources :approval_groups, :only => [:index, :show]
  resources :deployment_templates
  resources :dashboards, :only => [:index, :show, :home] do
    collection do
      get :home
    end
  end

  resources :approvals, :only => [:waiting] do
    collection do
      get :waiting
    end
  end

  namespace :admin do
    match '/' => 'admin#index', via: :get
    resources :approval_groups, :controllers => { :roles => 'admin/approval_groups' }
    resources :audits, only: [:index, :show]
    resources :dashboards, :controllers => { :dashboards => 'admin/dashboards' }, only: [:index]
    resources :deployment_modules, :controllers => { :roles => 'admin/deployment_modules' }, except: [:new, :create] do
      collection do
        get :upload
        post :perform_upload
      end

      member do
        get :upgrade
        patch :perform_upgrade
      end
    end
    resources :mailer_notification_permissions, :controllers => { :mailer_notification_permissions => 'admin/mailer_notification_permissions' }, :only => [:index]
    resources :roles, :controllers => { :roles => 'admin/roles' }, :only => [:index, :show]
    resources :users,  :controllers => { :users => 'admin/users'}
  end

  namespace :api, :defaults => {:format => :html} do
    root :to => 'api#index'

    resources :api, :controller => :api

    scope module: :v1, constraints: ApiConstraints.new(version: 1, default: true), :defaults => {:format => :json} do
      resources :applications, :only => [:index, :show] do
        member do
          put :watch
          delete :unwatch
        end
        resources :deployments, :only => [:index, :show, :deploy, :create, :update] do
          member do
            post :deploy
            put :watch
            delete :unwatch
          end
          resources :deployment_environments, :only => [:deploy, :destroy, :update_schedule, :cancel_schedule] do
            member do
              post :deploy
              delete :destroy
              patch :update_schedule
              delete :cancel_schedule
            end
            resources :approvals, :only => [:index, :show, :approve, :reject] do
              member do
                post :approve
                post :reject
              end
            end
          end
        end
        resources :environments, :only => [:index, :show, :destroy] do
          member do
            post :deploy
            delete :destroy
          end
        end
      end
      resources :audits, only: [:index, :show]
      resources :deployment_templates
      resources :deployment_modules, only: [:index, :show]
      resources :charts, only: [:approvals_per_deployment, :deployments_per_application] do
        collection do
          get :approvals_per_deployment
          get :deployments_per_application
          get :deployments_created_last_x_days
        end
      end
      resources :approvals, :only => [:waiting] do
        collection do
          get :waiting
        end
      end
      resources :users, only: [:index, :show]
    end
  end
end
